# PSONO App - Password Manager

[![Discord](https://img.shields.io/badge/Discord-join%20chat-738bd7.svg)](https://discord.gg/RuSvEjj)

# Canonical source

The canonical source of the PSONO App is [hosted on GitLab.com](https://gitlab.com/psono/psono-app).

# Documentation

The documentation for the Psono app can be found here:

[Psono Documentation](https://doc.psono.com/)


# Some hints:

to generate models:

    flutter pub run build_runner build

or permanent

    flutter pub run build_runner watch

