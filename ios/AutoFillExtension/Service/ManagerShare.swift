//
//  ManagerShare.swift
//  AutoFillExtension
//

import Foundation
import Sodium

class ManagerShareService {
        
    /**
     Returns an overview of all the share rights the user has any access to
     
     - Parameter completionHandler: The completion handler that will receive the list of datastores
     */
    public func readShareRightsOverview(completionHandler: @escaping (ReadShareRightsList?, Error?) -> Void) {
        
        return apiClient.readShareRightsOverview(
            token: storage.read(key: "token"),
            sessionSecretKey: storage.read(key: "sessionSecretKey"),
            completionHandler: completionHandler
        )
    }
    
    /**
     Returns an overview of all the share rights the user has any access to
     
     - Parameter completionHandler: The completion handler that will receive the list of datastores
     */
    public func readShare(shareId: String, secretKey: Bytes, completionHandler: @escaping (Share?, Error?) -> Void) {
        
        
        func intermediateCompletionHandler(data: ReadShare?, error: Error?) {
            
            guard data!.data != nil && data?.dataNonce != nil && data!.data!.count > 0 else {
                completionHandler(nil, nil)
                return
            }
            
            let dataJson: String? = cryptoLibrary.decryptData(
                text: data!.data!,
                nonce: data!.dataNonce!,
                secretKey: secretKey
            )
            
            guard let bodyJson = try? JSONSerialization.jsonObject(with: dataJson!.data(using: .utf8)!, options: JSONSerialization.ReadingOptions.mutableContainers) else {
                completionHandler(nil, nil)
                return
            }
            
            guard let dataDictionary: [String: Any] = bodyJson as? [String: Any] else {
                completionHandler(nil, nil)
                return
            }
            
            let rights =  [
                "read": data!.rights!.read,
                "write": data!.rights!.write,
                "grant": data!.rights!.grant,
            ]
            let share = Share.init(jsonDictionary: [
                "share_id": shareId as Any,
                "share_secret_key": converter.toHex(bytes: secretKey) as Any,
                "rights": rights
            ])
            
            if share != nil {
                if dataDictionary["type"] != nil {
                    share!.item = Item.init(jsonDictionary: dataDictionary)
                } else {
                    share!.folder = Folder.init(jsonDictionary: dataDictionary)
                }
            }
            
            completionHandler(share, nil)
        }
        
        return apiClient.readShare(
            token: storage.read(key: "token"),
            sessionSecretKey: storage.read(key: "sessionSecretKey"),
            shareId: shareId,
            completionHandler: intermediateCompletionHandler
        )
    }
}

let managerShare = ManagerShareService()
