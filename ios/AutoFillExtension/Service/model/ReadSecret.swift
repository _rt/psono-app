//
//  ReadSecret.swift
//  AutoFillExtension
//

import Foundation
import Sodium


class ReadSecret: JsonObject {
    public var createDate: String?
    public var writeDate: String?
    public var data: Bytes?
    public var dataNonce: Bytes?
    public var type: String?
    public var callbackUrl: String?
    public var callbackUser: String?
    public var callbackPass: String?
    
    required init() {}
    
    public required init?(jsonDictionary: [String: Any]){
        self.createDate = jsonDictionary["create_date"] as? String
        self.writeDate = jsonDictionary["write_date"] as? String
        self.data = converter.fromHex(string: jsonDictionary["data"] as? String)
        self.dataNonce = converter.fromHex(string: jsonDictionary["data_nonce"] as? String)
        self.type = jsonDictionary["type"] as? String
        self.callbackUrl = jsonDictionary["callback_url"] as? String
        self.callbackUser = jsonDictionary["callback_user"] as? String
        self.callbackPass = jsonDictionary["callback_pass"] as? String
    }
    
    public func toJsonDict() -> [String: Any] {
        
        var jsonDict: [String: Any] = [:]
        
        if self.data != nil { jsonDict["data"] = converter.toHex(bytes: self.data) }
        if self.dataNonce != nil { jsonDict["data_nonce"] = converter.toHex(bytes: self.dataNonce) }
        if self.type != nil { jsonDict["type"] = self.type }
        if self.callbackUrl != nil { jsonDict["callback_url"] = self.callbackUrl }
        if self.callbackUser != nil { jsonDict["callback_user"] = self.callbackUser }
        if self.callbackPass != nil { jsonDict["callback_pass"] = self.callbackPass }
        
        return jsonDict
    }
    
    public func toJson() -> String? {
        let jsonDict: [String: Any] = self.toJsonDict()
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: jsonDict, options: [])
            return String(data: jsonData, encoding: String.Encoding.utf8)
        } catch {
            return nil
        }
    }
}
