//
//  ApiClient.swift
//  AutoFillExtension
//

import Foundation

enum ApiClient: Error {
    
}

class ApiClientService {
    
    private var serverUrl:String!
    private var deviceFingerprint:String!
    
    public init() {
        self.serverUrl = storage.read(key:"serverUrl")
        self.deviceFingerprint = storage.read(key:"deviceFingerprint")
    }
    
    /**
     Trys to decrypt the data with the provided session secret key. Will return the data if the data for some reason could not be decryted
     
     - Parameter sessionSecretKey; The session secret keye that used to decrypt the data
     - Parameter data; The data itself that is supposed to be a json encoded dictionary with a text and nonce attribute
     
     - Returns: The decrypted data
     */
    public func decryptData(sessionSecretKey: String?, data: Data?) -> Data? {
        guard data != nil else {
            return data
        }
        guard sessionSecretKey != nil else {
            return data
        }
        
        guard let bodyJson = try? JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) else {
            return data
        }
        
        guard let jsonDictionary: [String: Any] = bodyJson as? [String: Any] else {
            return data
        }
        guard let text: String = jsonDictionary["text"] as? String else {
            return data
        }
        guard let nonce: String = jsonDictionary["nonce"] as? String else {
            return data
        }
        
        guard jsonDictionary["text"] != nil else {
            return data
        }
        
        guard jsonDictionary["nonce"] != nil else {
            return data
        }
        
        let decryptedBody: String? = cryptoLibrary.decryptData(
            text: converter.fromHex(string: text)!,
            nonce: converter.fromHex(string: nonce)!,
            secretKey: converter.fromHex(string: sessionSecretKey)!
        )
        
        return decryptedBody!.data(using: .utf8)
    }
    
    
    /**
     Small abstraction to encapsulated the standard
     
     - Parameter connectionType; The connection type like e.g. GET or POST
     - Parameter endpoint; The endpoint e.g. /datastore/
     - Parameter data: The data to send in the HTML body
     - Parameter headers: A dictionary of all the headers, usually contains the authorization token
     - Parameter completionHandler: The completion handler function that is called once the async request returns
     */
    public func call<T:JsonObject>(connectionType: String, endpoint: String, data: String?=nil, headers: [String: String]?=nil, sessionSecretKey: String?=nil, completionHandler: @escaping (T?, Error?) -> Void) {
        
        let request : NSMutableURLRequest = NSMutableURLRequest()
        
        request.httpMethod = connectionType
        request.url = URL(string: self.serverUrl + endpoint)
        
        if data != nil && sessionSecretKey != nil {
            // TODO encrypt data if any data is sent to the server. At the moment not necessary
        }
        if data != nil {
            request.httpBody = data!.data(using: .utf8)
        }
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        if sessionSecretKey != nil && headers != nil && headers!["Authorization"] != nil {
            let formatter = ISO8601DateFormatter()
            
            let validator = ["request_time": formatter.string(from: Date()), "request_device_fingerprint": self.deviceFingerprint]
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: validator, options: [])
                let jsonString = String(data: jsonData, encoding: String.Encoding.utf8)
                let (text, nonce) = cryptoLibrary.encryptData(data: jsonString!, secretKey: converter.fromHex(string: sessionSecretKey)!)
                let textNonceData = try JSONSerialization.data(withJSONObject: ["text": converter.toHex(bytes: text), "nonce": converter.toHex(bytes: nonce)], options: [])
                let textNonceString = String(data: textNonceData, encoding: String.Encoding.utf8)
                request.addValue(textNonceString!, forHTTPHeaderField: "Authorization-Validator")
            } catch {
                // pass
            }
        }
        
        if headers != nil {
            for (key, value) in headers! {
                request.addValue(value, forHTTPHeaderField: key)
            }
        }
        
        func intermediateCompletionHandler(data: Data?, response: URLResponse?, error: Error?) -> Void {
            if error != nil {
                completionHandler(nil, error)
                return
            }
            
            let decryptedData = decryptData(sessionSecretKey: sessionSecretKey, data: data)
            
            guard let bodyJson = try? JSONSerialization.jsonObject(with: decryptedData!, options: JSONSerialization.ReadingOptions.mutableContainers) else {
                completionHandler(nil, error)
                return
            }
            
            guard let jsonDictionary: [String: Any] = bodyJson as? [String: Any] else {
                completionHandler(nil, error)
                return
            }
            
            guard let jsonObject: T = T.init(jsonDictionary: jsonDictionary) else {
                completionHandler(nil, error)
                return
            }
            
            completionHandler(jsonObject, nil)
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: intermediateCompletionHandler)
        
        task.resume()
    }
    
    /**
     Reads a list of all the datastores from the servers
     
     - Parameter token; The session token to authorize the requiest
     - Parameter sessionSecretKey: The session secret key to encrypt the request and decrypt the response
     - Parameter completionHandler: The completion handler function that is called once the async request returns
     */
    public func readDatastoreList(token: String, sessionSecretKey: String, completionHandler: @escaping (ReadDatastoreList?, Error?) -> Void) {
        
        let connectionType: String = "GET"
        let endpoint: String = "/datastore/"
        let data: String? = nil
        let headers: [String: String]? = ["Authorization": "Token " + token]
        
        self.call(connectionType: connectionType, endpoint: endpoint, data: data, headers: headers, sessionSecretKey: sessionSecretKey, completionHandler: completionHandler)
    }
    
    /**
     Reads one datastore by its id from the server and returns its content
     
     - Parameter token; The session token to authorize the requiest
     - Parameter sessionSecretKey: The session secret key to encrypt the request and decrypt the response
     - Parameter datastoreId: The id of the datastore
     - Parameter completionHandler: The completion handler function that is called once the async request returns
     */
    public func readDatastore(token: String, sessionSecretKey: String, datastoreId: String, completionHandler: @escaping (ReadDatastore?, Error?) -> Void) {
        
        let connectionType: String = "GET"
        let endpoint: String = "/datastore/" + datastoreId + "/"
        let data: String? = nil
        let headers: [String: String]? = ["Authorization": "Token " + token]
        
        self.call(connectionType: connectionType, endpoint: endpoint, data: data, headers: headers, sessionSecretKey: sessionSecretKey, completionHandler: completionHandler)
    }
    
    /**
     Reads one secret  by its id from the server and returns its content
     
     - Parameter token; The session token to authorize the requiest
     - Parameter sessionSecretKey: The session secret key to encrypt the request and decrypt the response
     - Parameter secretId: The id of the secret
     - Parameter completionHandler: The completion handler function that is called once the async request returns
     */
    public func readSecret(token: String, sessionSecretKey: String, secretId: String, completionHandler: @escaping (ReadSecret?, Error?) -> Void) {
        
        let connectionType: String = "GET"
        let endpoint: String = "/secret/" + secretId + "/"
        let data: String? = nil
        let headers: [String: String]? = ["Authorization": "Token " + token]
        
        self.call(connectionType: connectionType, endpoint: endpoint, data: data, headers: headers, sessionSecretKey: sessionSecretKey, completionHandler: completionHandler)
    }
    
    /**
     Reads one share  by its id from the server and returns its content
     
     - Parameter token; The session token to authorize the requiest
     - Parameter sessionSecretKey: The session secret key to encrypt the request and decrypt the response
     - Parameter shareId: The id of the share
     - Parameter completionHandler: The completion handler function that is called once the async request returns
     */
    public func readShare(token: String, sessionSecretKey: String, shareId: String, completionHandler: @escaping (ReadShare?, Error?) -> Void) {
        
        let connectionType: String = "GET"
        let endpoint: String = "/share/" + shareId + "/"
        let data: String? = nil
        let headers: [String: String]? = ["Authorization": "Token " + token]
        
        self.call(connectionType: connectionType, endpoint: endpoint, data: data, headers: headers, sessionSecretKey: sessionSecretKey, completionHandler: completionHandler)
    }
    
    /**
     Reads all the share rights that the user has access to
     
     - Parameter token; The session token to authorize the requiest
     - Parameter sessionSecretKey: The session secret key to encrypt the request and decrypt the response
     - Parameter completionHandler: The completion handler function that is called once the async request returns
     */
    public func readShareRightsOverview(token: String, sessionSecretKey: String, completionHandler: @escaping (ReadShareRightsList?, Error?) -> Void) {
        
        let connectionType: String = "GET"
        let endpoint: String = "/share/right/"
        let data: String? = nil
        let headers: [String: String]? = ["Authorization": "Token " + token]
        
        self.call(connectionType: connectionType, endpoint: endpoint, data: data, headers: headers, sessionSecretKey: sessionSecretKey, completionHandler: completionHandler)
    }
}

let apiClient = ApiClientService()
