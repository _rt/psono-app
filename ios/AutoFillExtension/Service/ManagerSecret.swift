//
//  ManagerSecret.swift
//  AutoFillExtension
//

import Foundation
import Sodium

class ManagerSecretService {
    
    public func readSecret(secretId: String, secretKey: Bytes, completionHandler: @escaping (Secret?, Error?) -> Void) {
        
        func helperCompletionHandler(secret: ReadSecret?, error: Error?) -> Void {
            
            guard secret!.data != nil && secret?.dataNonce != nil && secret!.data!.count > 0 else {
                completionHandler(nil, nil)
                return
            }
            
            let dataJson: String? = cryptoLibrary.decryptData(
                text: secret!.data!,
                nonce: secret!.dataNonce!,
                secretKey: secretKey
            )
            
            guard let bodyJson = try? JSONSerialization.jsonObject(with: dataJson!.data(using: .utf8)!, options: JSONSerialization.ReadingOptions.mutableContainers) else {
                completionHandler(nil, nil)
                return
            }
            
            guard let dataDictionary: [String: Any] = bodyJson as? [String: Any] else {
                completionHandler(nil, nil)
                return
            }
            
            let decodedSecret = Secret.init(jsonDictionary: [
                "create_date": secret?.createDate as Any,
                "write_date": secret?.writeDate as Any,
                "secret_id": secretId as Any,
                "type": secret?.type as Any,
                "data": dataDictionary,
                "callback_url": secret?.callbackUrl as Any,
                "callback_user": secret?.callbackUser as Any,
                "callback_pass": secret?.callbackPass as Any,
                "secret_key": converter.toHex(bytes: secretKey) as Any,
            ])
            
            completionHandler(decodedSecret, nil)
        }
            
        return apiClient.readSecret(
            token: storage.read(key: "token"),
            sessionSecretKey: storage.read(key: "sessionSecretKey"),
            secretId: secretId,
            completionHandler: helperCompletionHandler
        )
    }
}

let managerSecret = ManagerSecretService()
