import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'package:psono/model/share_right.dart';
import 'package:psono/model/otp.dart';
import 'package:psono/model/datastore.dart' as datastoreModel;
import 'package:psono/services/helper.dart' as helper;
import 'package:psono/services/item_blueprint.dart' as itemBlueprint;
import 'package:psono/services/crypto_library.dart' as cryptoLibary;
import 'package:psono/services/manager_secret.dart' as managerSecret;
import 'package:psono/model/share.dart' as shareModel;
import 'package:psono/services/manager_datastore_password.dart'
    as managerDatastorePassword;
import 'package:psono/services/manager_share.dart' as managerShare;
import 'package:psono/screens/custom_drawer.dart';
import 'package:psono/screens/scan_qr/index.dart';
import 'package:psono/model/secret.dart';
import 'package:psono/components/_index.dart' as component;

class AddItemScreen extends StatefulWidget {
  static String tag = 'add-item-screen';
  final datastoreModel.Folder parent;
  final datastoreModel.Datastore datastore;
  final datastoreModel.Folder share;
  final List<String> path;
  final List<String> relativePath;

  AddItemScreen(
      {this.parent, this.datastore, this.share, this.path, this.relativePath});

  @override
  _AddItemScreenState createState() => _AddItemScreenState();
}

class _AddItemScreenState extends State<AddItemScreen> {
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  String type;
  String _screen = 'default';
  Secret _secret = new Secret(
    data: {},
  );
  bool _obscurePassword = true;
  bool _advanced = false;
  OTP otp;
  List<TextEditingController> controllers = [];

  @override
  void dispose() {
    for (var i = 0; i < controllers.length; i++) {
      controllers[i]?.dispose();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> menuList = [];

    List blueprints = itemBlueprint.getBlueprints();

    blueprints.forEach((blueprint) {
      menuList.add(
        Card(
          child: ListTile(
            title: Text(FlutterI18n.translate(context, blueprint.name)),
            leading: Icon(blueprint.icon),
            trailing: Icon(Icons.keyboard_arrow_right),
            onTap: () async {
              if (blueprint.id == 'totp') {
                final String result = await Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ScanQR()),
                );
                otp = helper.parseTOTPUri(result);
              }
              setState(() {
                type = blueprint.id;
              });
            },
          ),
        ),
      );
    });

    if (_screen == 'loading') {
      return Scaffold(
        backgroundColor: Color(0xFF151f2b),
        body: component.Loading(),
      );
    } else if (type != null) {
      var _bp = itemBlueprint.getBlueprint(type);

      int getFieldLength() {
        if (_advanced) {
          return _bp.fields.length + 1;
        } else {
          int advancedCount =
              _bp.fields.where((field) => field.position == 'advanced').length;
          return _bp.fields.length - advancedCount + 1;
        }
      }

      return Scaffold(
        appBar: AppBar(
          title: Text(FlutterI18n.translate(
                context,
                'NEW',
              ) +
              ' ' +
              FlutterI18n.translate(
                context,
                _bp.name,
              )),
          elevation: 0,
          backgroundColor: Colors.white,
        ),
        backgroundColor: Color(0xFFebeeef),
        body: new Container(
          padding: new EdgeInsets.all(20.0),
          child: new Form(
            key: this._formKey,
            child: ListView.builder(
              // Let the ListView know how many items it needs to build.
              itemCount: getFieldLength(),
              // Provide a builder function. This is where the magic happens.
              // Convert each item into a widget based on the type of item it is.
              itemBuilder: (c, index) {
                if (index == getFieldLength() - 1) {
                  // last element, render the save button

                  return Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16.0),
                    child: component.BtnSuccess(
                      onPressed: () async {
                        if (!_formKey.currentState.validate()) {
                          return;
                        }
                        setState(() {
                          _screen = 'loading';
                        });
                        if (this.widget.parent.items == null) {
                          this.widget.parent.items = [];
                        }

                        _secret.type = this.type;

                        String linkId = await cryptoLibary.generateUUID();

                        String parentDatastoreId;
                        String parentShareId;
                        if (this.widget.share == null) {
                          parentDatastoreId = this.widget.datastore.datastoreId;
                        } else {
                          parentShareId = this.widget.share.shareId;
                        }
                        String callbackUrl = '';
                        String callbackUser = '';
                        String callbackPass = '';

                        Secret createdSecret = await managerSecret.createSecret(
                          _secret.data,
                          linkId,
                          parentDatastoreId,
                          parentShareId,
                          callbackUrl,
                          callbackUser,
                          callbackPass,
                        );
                        _secret.secretId = createdSecret.secretId;
                        _secret.secretKey = createdSecret.secretKey;

                        datastoreModel.Item newItem = new datastoreModel.Item(
                          id: linkId,
                          name: _secret.data[_bp.titleField],
                          type: this.type,
                          secretId: createdSecret.secretId,
                          secretKey: createdSecret.secretKey,
                        );

                        if (_bp.urlfilterField != null) {
                          newItem.urlfilter = _secret.data[_bp.urlfilterField];
                        }

                        if (this.widget.share == null) {
                          newItem.shareRights = new ShareRight(
                            read: true,
                            write: true,
                            grant: true,
                            delete: true,
                          );
                        } else {
                          newItem.shareRights = new ShareRight(
                            read: this.widget.share.shareRights.read,
                            write: this.widget.share.shareRights.write,
                            grant: this.widget.share.shareRights.grant &&
                                this.widget.share.shareRights.write,
                            delete: this.widget.share.shareRights.write,
                          );
                        }

                        this.widget.parent.items.add(newItem);

                        if (this.widget.share == null) {
                          datastoreModel.Datastore datastore =
                              await managerDatastorePassword
                                  .getPasswordDatastore(
                            this.widget.datastore.datastoreId,
                          );

                          datastoreModel.Folder parent;
                          if (this.widget.path.length == 0) {
                            parent = datastore.data;
                          } else {
                            List<String> pathCopy =
                                new List<String>.from(this.widget.path);
                            List search =
                                managerDatastorePassword.findInDatastore(
                              pathCopy,
                              datastore.data,
                            );
                            parent = search[0][search[1]];
                          }

                          if (parent.items == null) {
                            parent.items = [];
                          }
                          parent.items.add(newItem);
                          await datastore.save();
                        } else {
                          shareModel.Share share = await managerShare.readShare(
                            this.widget.share.shareId,
                            this.widget.share.shareSecretKey,
                          );
                          datastoreModel.Folder parent;
                          if (this.widget.relativePath.length == 0) {
                            parent = share.folder;
                          } else {
                            List<String> pathCopy = new List<String>.from(
                              this.widget.relativePath,
                            );
                            List search =
                                managerDatastorePassword.findInDatastore(
                              pathCopy,
                              share.folder,
                            );
                            parent = search[0][search[1]];
                          }
                          if (parent.items == null) {
                            parent.items = [];
                          }
                          parent.items.add(newItem);
                          await share.save();
                        }
                        setState(() {
                          _screen = 'default';
                        });

                        Navigator.pop(context);
                        return;
                      },
                      text: FlutterI18n.translate(context, "SAVE"),
                    ),
                  );
                } else {
                  // render field
                  final field = _bp.fields[index];

                  String content = '';
                  if (_secret.data.containsKey(field.name)) {
                    content = _secret.data[field.name];
                  }
                  if (_bp.id == 'totp' && this.otp != null && content == '') {
                    if (field.name == 'totp_title') {
                      content = this.otp.getDescription();
                      _secret.data[field.name] = content;
                    }
                    if (field.name == 'totp_code') {
                      content = this.otp.secret;
                      _secret.data[field.name] = content;
                    }
                  }

                  TextInputType keyboardType;
                  bool obscureText = false;
                  int maxLines = 1;

                  List<DropdownMenuItem<String>> buttons = [];

                  if (field.field == 'input' && field.type == 'text') {
                  } else if (field.field == 'input' &&
                      field.type == 'password') {
                    obscureText = true && _obscurePassword;
                    buttons.add(DropdownMenuItem<String>(
                      value: "show-hide",
                      child: Row(children: <Widget>[
                        Icon(
                          _obscurePassword
                              ? Icons.visibility
                              : Icons.visibility_off,
                          color: Theme.of(context).primaryColorDark,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 12.0),
                          child: Text(
                            _obscurePassword
                                ? FlutterI18n.translate(
                                    context, "SHOW_PASSWORD")
                                : FlutterI18n.translate(
                                    context, "HIDE_PASSWORD"),
                          ),
                        ),
                      ]),
                    ));
                    buttons.add(DropdownMenuItem<String>(
                      value: "generate",
                      child: Row(children: <Widget>[
                        Icon(
                          component.FontAwesome.cogs,
                          color: Theme.of(context).primaryColorDark,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 12.0),
                          child: Text(
                            FlutterI18n.translate(context, "GENERATE_PASSWORD"),
                          ),
                        ),
                      ]),
                    ));
                  } else if (field.field == 'input' &&
                      field.type == 'totp_code') {
                    obscureText = true && _obscurePassword;
                    buttons.add(DropdownMenuItem<String>(
                      value: "show-hide",
                      child: Row(children: <Widget>[
                        Icon(
                          _obscurePassword
                              ? Icons.visibility
                              : Icons.visibility_off,
                          color: Theme.of(context).primaryColorDark,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 12.0),
                          child: Text(
                            _obscurePassword
                                ? FlutterI18n.translate(context, "SHOW")
                                : FlutterI18n.translate(context, "HIDE"),
                          ),
                        ),
                      ]),
                    ));
                    buttons.add(DropdownMenuItem<String>(
                      value: "scan-totp-code",
                      child: Row(children: <Widget>[
                        Icon(
                          component.FontAwesome.qrcode,
                          color: Theme.of(context).primaryColorDark,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 12.0),
                          child: Text(
                            FlutterI18n.translate(context, "SCAN_QR_CODE"),
                          ),
                        ),
                      ]),
                    ));
                  } else if (field.field == 'input' &&
                      field.type == 'checkbox') {
                  } else if (field.field == 'textarea') {
                    keyboardType = TextInputType.multiline;
                    maxLines = null;
                  } else if (field.field == 'button' &&
                      field.type == 'button') {
                    keyboardType = TextInputType.multiline;
                  } else {
                    throw ("unknown field type combi");
                  }

                  final textController = TextEditingController(
                    text: content,
                  );

                  controllers.add(textController);

                  List<Widget> children = [];
                  children.add(
                    Stack(
                      alignment: Alignment.centerRight,
                      children: [
                        TextFormField(
                          obscureText: obscureText,
                          controller: textController,
                          validator: (value) {
                            if (field.required && value.isEmpty) {
                              return FlutterI18n.translate(
                                  context, field.errorMessageRequired);
                            }
                            if (field.field == 'input' &&
                                field.type == 'totp_code' &&
                                !helper.isValidTOTPCode(value)) {
                              return FlutterI18n.translate(
                                  context, 'INVALID_SECRET');
                            }
                            return null;
                          },
                          keyboardType: keyboardType,
                          maxLines: maxLines,
                          onChanged: (text) {
                            _secret.data[field.name] = text;
                            if (field.field == 'input' &&
                                field.type == 'totp_code') {}
                          },
                          decoration: new InputDecoration(
                              contentPadding:
                                  EdgeInsets.fromLTRB(0, 10, 48.0, 10),
                              labelText:
                                  FlutterI18n.translate(context, field.title)),
                        ),
                        new DropdownButtonHideUnderline(
                          child: DropdownButton<String>(
                            icon: Icon(component.FontAwesome.ellipsis_v),
                            iconSize: 20,
                            dropdownColor: Colors.white,
                            onChanged: (String newValue) async {
                              if (newValue == 'show-hide') {
                                setState(() {
                                  _obscurePassword = !_obscurePassword;
                                });
                              }
                              if (newValue == 'generate') {
                                textController.text =
                                    await managerDatastorePassword.generate();
                                _secret.data[field.name] = textController.text;
                              }
                              if (newValue == 'scan-totp-code') {
                                final String result = await Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => ScanQR()),
                                );
                                var otp = helper.parseTOTPUri(result);
                                _secret.data[field.name] = otp.secret;
                                textController.text = otp.secret;
                              }
                            },
                            items: buttons,
                          ),
                        )
                      ],
                    ),
                  );

                  return new Container(
                    child: new Column(
                      children: children,
                    ),
                  );
                }
              },
            ),
          ),
        ),
        drawer: CustomDrawer(),
      );
    } else {
      return Scaffold(
        appBar: AppBar(
          title: Text(FlutterI18n.translate(context, 'NEW_ENTRY')),
          elevation: 0,
          backgroundColor: Colors.white,
        ),
        backgroundColor: Color(0xFFebeeef),
        body: new Container(
            padding: new EdgeInsets.all(10.0),
            child: new ListView(
              children: ListTile.divideTiles(
                context: context,
                tiles: menuList,
              ).toList(),
            )),
        drawer: CustomDrawer(),
      );
    }
  }
}
