import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';

import 'package:psono/model/datastore.dart' as datastoreModel;
import 'package:psono/screens/add_folder/index.dart';
import 'package:psono/screens/add_item/index.dart';
import 'package:psono/theme.dart';

class CustomFloatingActionButton extends StatefulWidget {
  static String tag = 'custom-floating-action-button';
  final datastoreModel.Folder parent;
  final datastoreModel.Datastore datastore;
  final datastoreModel.Folder share;
  final List<String> path;
  final List<String> relativePath;
  final RefreshCallback onRefresh;

  CustomFloatingActionButton(
      {this.parent,
      this.datastore,
      this.share,
      this.path,
      this.relativePath,
      this.onRefresh});

  @override
  _CustomFloatingActionButtonState createState() =>
      _CustomFloatingActionButtonState();
}

class _CustomFloatingActionButtonState
    extends State<CustomFloatingActionButton> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SpeedDial(
      // both default to 16
      marginRight: 18,
      marginBottom: 20,
      animatedIcon: AnimatedIcons.menu_close,
      animatedIconTheme: IconThemeData(size: 24.0),
      closeManually: false,
      curve: Curves.bounceIn,
      overlayColor: Colors.black,
      overlayOpacity: 0.5,
      tooltip: FlutterI18n.translate(context, "ADD"),
      heroTag: 'speed-dial-hero-tag',
      backgroundColor: primarySwatch.shade500,
      foregroundColor: Colors.black,
      shape: CircleBorder(),
      children: [
        SpeedDialChild(
          child: Icon(Icons.add),
          backgroundColor: Colors.white,
          label: FlutterI18n.translate(context, "NEW_ENTRY"),
          labelStyle: TextStyle(fontSize: 18.0),
          onTap: () async {
            await Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => AddItemScreen(
                  parent: this.widget.parent,
                  datastore: this.widget.datastore,
                  share: this.widget.share,
                  path: this.widget.path,
                  relativePath: this.widget.relativePath,
                ),
              ),
            );
            this.widget.onRefresh();
          },
        ),
        SpeedDialChild(
          child: Icon(Icons.folder),
          backgroundColor: Colors.white,
          label: FlutterI18n.translate(context, "NEW_FOLDER"),
          labelStyle: TextStyle(fontSize: 18.0),
          onTap: () async {
            await Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => AddFolderScreen(
                  parent: this.widget.parent,
                  datastore: this.widget.datastore,
                  share: this.widget.share,
                  path: this.widget.path,
                  relativePath: this.widget.relativePath,
                ),
              ),
            );
            this.widget.onRefresh();
          },
        ),
      ],
    );
  }
}
