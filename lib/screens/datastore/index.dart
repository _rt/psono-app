import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'package:psono/components/_index.dart' as component;
import 'package:psono/theme.dart';
import 'package:psono/screens/custom_drawer.dart';
import 'package:psono/screens/custom_floating_action_button.dart';
import 'package:psono/screens/edit_folder/index.dart';
import 'package:psono/screens/edit_secret/index.dart';
import 'package:psono/screens/folder/index.dart';
import 'package:psono/services/api_client/index.dart' as apiClient;
import 'package:psono/services/manager_datastore_user.dart'
    as managerDatastoreUser;
import 'package:psono/services/manager_widget.dart' as managerWidget;
import 'package:psono/model/datastore.dart' as datastoreModel;
import 'package:psono/services/manager_datastore_password.dart'
    as managerDatastorePassword;

class DatastoreScreen extends StatefulWidget {
  static String tag = 'datastore-screen';
  final List<String> autoNavigate;

  const DatastoreScreen({this.autoNavigate});

  @override
  _DatastoreScreenState createState() => _DatastoreScreenState();
}

class _DatastoreScreenState extends State<DatastoreScreen> {
  String defaultSearch;
  String _search;
  String _screen = 'default';
  datastoreModel.Datastore _datastore;
  datastoreModel.Folder _root;
  datastoreModel.Folder _share;
  Widget appBarTitle = Text("Datastore");
  Icon actionIcon = new Icon(Icons.search);
  final List<String> path = [];
  final List<String> relativePath = [];
  List<String> selectedItems = [];
  List<String> selectedFolders = [];

  static const sharedDataChannel =
      const MethodChannel('com.psono.psono/psono_shared_data');
  bool autofill = false;

  Future<void> loadDatastore() async {
    datastoreModel.Datastore datastore;
    try {
      datastore = await managerDatastorePassword.getPasswordDatastore();
    } on apiClient.ServiceUnavailableException {
      if (!mounted) return;
      setState(() {
        _screen = 'offline';
      });
      return;
    } on apiClient.UnauthorizedException {
      managerDatastoreUser.logout();
      Navigator.pushReplacementNamed(context, '/signin/');
      return;
    }
    if (!mounted) return;
    setState(() {
      _screen = 'default';
      _datastore = datastore;
      _root = datastore.data;
      _share = null;
    });
  }

  Future<void> initStateAsync() async {
    var sharedData = await sharedDataChannel.invokeMethod("getSharedText");
    if (sharedData != null) {
      List<String> sharedDataList = sharedData.split("::");
      print(sharedDataList);
      if (sharedDataList.length == 3 && sharedDataList[0] == 'autofill') {
        setState(() {
          autofill = true;
        });
        if (sharedDataList[2].length > 0) {
          setState(() {
            defaultSearch = sharedDataList[2];
          });
        }
      }
    }

    await loadDatastore();

    if (this.widget.autoNavigate != null &&
        this.widget.autoNavigate.length > 0) {
      List<String> newAutoNavigate = List.from(this.widget.autoNavigate);
      String folderId = newAutoNavigate.removeAt(0);

      for (var i = 0; i < _root.folders.length; i++) {
        if (_root.folders[i].id != folderId) {
          continue;
        }
        datastoreModel.Folder newShare;
        List<String> newRelativePath;
        if (_root.folders[i].shareId != null) {
          newShare = _root.folders[i];
          newRelativePath = [];
        } else {
          newRelativePath = List.from(relativePath)
            ..addAll([_root.folders[i].id]);
        }

        Navigator.push(
          context,
          NoAnimationMaterialPageRoute(
            builder: (context) => FolderScreen(
              autoNavigate: newAutoNavigate,
              folder: _root.folders[i],
              datastore: _datastore,
              share: newShare,
              path: List.from(path)..addAll([_root.folders[i].id]),
              relativePath: newRelativePath,
              autofill: autofill,
            ),
          ),
        );
        return;
      }
    }
  }

  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      initStateAsync();
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Future _handleRefresh() async {
      Navigator.pushReplacement(
        context,
        NoAnimationMaterialPageRoute(
          builder: (context) => DatastoreScreen(
            autoNavigate: [],
          ),
        ),
      );
    }

    Future _handleLogout() async {
      managerDatastoreUser.logout();
      Navigator.pushReplacementNamed(context, '/signin/');
    }

    void searchCallback(String search) {
      setState(() {
        defaultSearch = null;
        _search = search;
      });
    }

    void deleteCallback() async {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: new Text(FlutterI18n.translate(context, 'DELETE_ENTRY')),
            content: new Text(
                FlutterI18n.translate(context, 'DELETE_ENTRY_WARNING')),
            actions: <Widget>[
              new FlatButton(
                child: new Text(FlutterI18n.translate(context, "ABORT")),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              new component.BtnPrimary(
                onPressed: () async {
                  await managerWidget.deleteItem(
                    selectedItems,
                    selectedFolders,
                    relativePath,
                    _share,
                    _datastore,
                    'password',
                  );

                  setState(() {
                    selectedItems = [];
                    selectedFolders = [];
                  });

                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                      builder: (context) => DatastoreScreen(
                        autoNavigate: [],
                      ),
                    ),
                  );
                },
                text: FlutterI18n.translate(context, "YES"),
              ),
            ],
          );
        },
      );
    }

    void editCallback() async {
      if (selectedItems.length == 1) {
        // Trigger edit of an item
        datastoreModel.Item item =
            _root.items.where((item) => selectedItems.contains(item.id)).first;
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => EditSecretScreen(
              parent: _root,
              datastore: _datastore,
              share: _share,
              item: item,
              path: path,
              relativePath: relativePath,
            ),
          ),
        );
      } else if (selectedFolders.length == 1) {
        // Trigger edit of a folder
        datastoreModel.Folder folder = _root.folders
            .where((folder) => selectedFolders.contains(folder.id))
            .first;
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => EditFolderScreen(
              parent: _root,
              datastore: _datastore,
              share: _share,
              folder: folder,
              path: path,
              relativePath: relativePath,
            ),
          ),
        );
      }
    }

    Widget floatingActionButton;

    if (_screen == 'offline') {
      List<Widget> buttons = [
        component.BtnPrimary(
          text: FlutterI18n.translate(
            context,
            "RETRY",
          ),
          onPressed: _handleRefresh,
        ),
        Padding(
          padding: EdgeInsets.only(left: 10),
          child: component.Btn(
            text: FlutterI18n.translate(
              context,
              "LOGOUT",
            ),
            onPressed: _handleLogout,
          ),
        )
      ];

      return Scaffold(
        backgroundColor: Color(0xFF151f2b),
        body: new Container(
          child: component.ServerOffline(
            buttons: buttons,
          ),
        ),
      );
    } else {
      Widget contentView;
      if (_search == null) {
        if (selectedItems.length == 0 && selectedFolders.length == 0) {
          contentView = component.FolderTree(
            root: _root,
            datastore: _datastore,
            share: _share,
            path: path,
            relativePath: relativePath,
            autofill: autofill,
            onLongPressFolder: (datastoreModel.Folder folder) async {
              if (!folder.shareRights.delete) {
                return;
              }
              setState(() {
                selectedItems = [];
                selectedFolders = [folder.id];
              });
            },
            onLongPressItem: (datastoreModel.Item item) async {
              if (!item.shareRights.delete) {
                return;
              }
              setState(() {
                selectedItems = [item.id];
                selectedFolders = [];
              });
            },
          );

          floatingActionButton = CustomFloatingActionButton(
            parent: _root,
            datastore: _datastore,
            share: _share,
            path: path,
            relativePath: relativePath,
            onRefresh: _handleRefresh,
          );
        } else {
          contentView = component.FolderSelectTree(
            root: _root,
            datastore: _datastore,
            share: _share,
            path: path,
            relativePath: relativePath,
            selectedFolders: selectedFolders,
            selectedItems: selectedItems,
            onSelectFolder: (datastoreModel.Folder folder) async {
              setState(() {
                if (selectedFolders.contains(folder.id)) {
                  selectedFolders.remove(folder.id);
                } else {
                  if (!folder.shareRights.delete) {
                    return;
                  }
                  selectedFolders.add(folder.id);
                }
              });
            },
            onSelectItem: (datastoreModel.Item item) async {
              setState(() {
                if (selectedItems.contains(item.id)) {
                  selectedItems.remove(item.id);
                } else {
                  if (!item.shareRights.delete) {
                    return;
                  }
                  selectedItems.add(item.id);
                }
              });
            },
          );
        }
      } else {
        contentView = component.FolderSearchTree(
          root: _root,
          datastore: _datastore,
          share: _share,
          path: path,
          relativePath: relativePath,
          search: _search,
          autofill: autofill,
        );
      }

      Widget appBar;
      if (selectedItems.length + selectedFolders.length > 0) {
        appBar = component.SliverAppMultiselectBar(
          onDelete: deleteCallback,
          onEdit: editCallback,
          selectedElements: selectedItems.length + selectedFolders.length,
        );
      } else {
        appBar = component.SliverAppSearchBar(
          title: FlutterI18n.translate(context, "DATASTORE"),
          onSearch: searchCallback,
          defaultSearch: defaultSearch,
        );
      }

      return Scaffold(
        backgroundColor: Color(0xFFebeeef),
        body: new RefreshIndicator(
          onRefresh: _handleRefresh,
          child: CustomScrollView(
            slivers: <Widget>[
              appBar,
              contentView,
            ],
          ),
        ),
        drawer: CustomDrawer(),
        floatingActionButton: floatingActionButton,
      );
    }
  }
}
