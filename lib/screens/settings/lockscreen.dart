import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter/services.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/redux/actions.dart';
import 'package:psono/redux/store.dart';
import 'package:psono/services/storage.dart';

class SettingLockScreen extends StatefulWidget {
  static String tag = 'settings-lock-screen';

  @override
  _SettingLockScreenState createState() => _SettingLockScreenState();
}

class _SettingLockScreenState extends State<SettingLockScreen> {
  final passphrase = TextEditingController(
    text: '',
  );
  final passphraseRepeat = TextEditingController(
    text: '',
  );

  bool enabled = false;
  String message;

  @override
  void initState() {
    super.initState();
    if (reduxStore.state.lockscreenEnabled == true) {
      enabled = true;
    }
    if (reduxStore.state.lockscreenPassphrase != null) {
      passphrase.text = reduxStore.state.lockscreenPassphrase;
      passphraseRepeat.text = reduxStore.state.lockscreenPassphrase;
    }
  }

  @override
  void dispose() {
    passphrase?.dispose();
    passphraseRepeat?.dispose();
    super.dispose();
  }

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    Widget messageWidget = Container();
    if (message != null) {
      messageWidget = component.AlertInfo(
        text: message,
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(FlutterI18n.translate(
          context,
          'LOCKSCREEN',
        )),
        elevation: 0,
        backgroundColor: Colors.white,
      ),
      backgroundColor: Color(0xFFebeeef),
      body: new Container(
        padding: new EdgeInsets.all(20.0),
        child: Form(
          key: _formKey,
          child: ListView(
            children: <Widget>[
              new TextFormField(
                obscureText: true,
                controller: passphrase,
                validator: (value) {
                  if (value.isEmpty) {
                    return FlutterI18n.translate(
                      context,
                      'PASSPHRASE_REQUIRED',
                    );
                  }
                  return null;
                },
                inputFormatters: <TextInputFormatter>[
                  FilteringTextInputFormatter.digitsOnly,
                  LengthLimitingTextInputFormatter(8)
                ],
                keyboardType: TextInputType.number,
                decoration: new InputDecoration(
                  labelText: FlutterI18n.translate(
                    context,
                    'PASSPHRASE',
                  ),
                ),
              ),
              new TextFormField(
                obscureText: true,
                controller: passphraseRepeat,
                validator: (value) {
                  if (value.isEmpty) {
                    return FlutterI18n.translate(
                      context,
                      'PASSPHRASE_REQUIRED',
                    );
                  }
                  if (value != passphrase.text) {
                    return FlutterI18n.translate(
                      context,
                      'PASSPHRASE_MISSMATCH',
                    );
                  }
                  return null;
                },
                inputFormatters: <TextInputFormatter>[
                  FilteringTextInputFormatter.digitsOnly,
                  LengthLimitingTextInputFormatter(8)
                ],
                keyboardType: TextInputType.number,
                decoration: new InputDecoration(
                  labelText: FlutterI18n.translate(
                    context,
                    'PASSPHRASE_REPEAT',
                  ),
                ),
              ),
              new CheckboxListTile(
                title: Text(FlutterI18n.translate(context, "ACTIVE")),
                value: enabled,
                onChanged: (value) {
                  setState(() {
                    enabled = value;
                  });
                },
                controlAffinity: ListTileControlAffinity.leading,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: component.BtnSuccess(
                  onPressed: () async {
                    if (!_formKey.currentState.validate()) {
                      return;
                    }
                    await storage.write(
                      key: 'lockscreenPassphrase',
                      value: passphrase.text,
                      iOptions: secureIOSOptions,
                    );

                    await storage.write(
                      key: 'lockscreenEnabled',
                      value: enabled.toString(),
                      iOptions: secureIOSOptions,
                    );

                    reduxStore.dispatch(
                      new UpdateLockscreenSettingAction(
                        passphrase.text,
                        enabled,
                      ),
                    );
                    setState(() {
                      message = FlutterI18n.translate(context, "SAVE_SUCCESS");
                    });
                  },
                  text: FlutterI18n.translate(context, "SAVE"),
                ),
              ),
              messageWidget,
            ],
          ),
        ),
      ),
    );
  }
}
