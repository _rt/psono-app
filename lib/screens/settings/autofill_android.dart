import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/services/autofill.dart' as autofillService;

class SettingAutofillAndroidScreen extends StatefulWidget {
  static String tag = 'settings-autofill-screen';

  @override
  _SettingAutofillAndroidScreenState createState() =>
      _SettingAutofillAndroidScreenState();
}

class _SettingAutofillAndroidScreenState
    extends State<SettingAutofillAndroidScreen> {
  bool enabled = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    SchedulerBinding.instance.addPostFrameCallback((_) async {
      bool autofillEnabled = await autofillService.isEnabled();
      if (enabled != autofillEnabled) {
        setState(() {
          enabled = autofillEnabled;
        });
      }
    });

    Widget button;
    if (enabled) {
      button = component.BtnWarning(
        onPressed: () async {
          autofillService.disable();
          setState(() {
            enabled = false;
          });
        },
        text: FlutterI18n.translate(context, "DEACTIVATE"),
      );
    } else {
      button = component.BtnSuccess(
        onPressed: () async {
          autofillService.enable();
          Navigator.pop(context);
        },
        text: FlutterI18n.translate(context, "ACTIVATE"),
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(FlutterI18n.translate(
          context,
          'AUTOFILL_SERVICE',
        )),
        elevation: 0,
        backgroundColor: Colors.white,
      ),
      backgroundColor: Color(0xFFebeeef),
      body: new Container(
        padding: new EdgeInsets.all(20.0),
        child: Form(
          key: _formKey,
          child: ListView(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: button,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
