import 'dart:convert';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

import 'package:psono/components/_index.dart' as component;
import 'package:psono/model/config.dart';
import 'package:psono/redux/actions.dart';
import 'package:psono/redux/store.dart';
import 'package:psono/services/converter.dart' as converter;
import 'package:psono/services/autofill.dart' as autofillService;
import 'package:psono/services/manager_host.dart';
import 'package:psono/services/storage.dart';

class HomeScreen extends StatefulWidget {
  static String tag = 'signin-screen';
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    SchedulerBinding.instance.addPostFrameCallback((_) async {
      String serverUrl = await storage.read(key: 'serverUrl');
      String username = await storage.read(key: 'username');
      String token = await storage.read(key: 'token');
      String sessionSecretKey = await storage.read(key: 'sessionSecretKey');
      String secretKey = await storage.read(key: 'secretKey');
      String privateKey = await storage.read(key: 'privateKey');
      String publicKey = await storage.read(key: 'publicKey');
      String lockscreenPassphrase =
          await storage.read(key: 'lockscreenPassphrase');
      bool lockscreenEnabled =
          (await storage.read(key: 'lockscreenEnabled')) == 'true';
      bool complianceDisableDeleteAccount =
          (await storage.read(key: 'complianceDisableDeleteAccount')) == 'true';
      String userId = await storage.read(key: 'userId');
      String userEmail = await storage.read(key: 'userEmail');
      String userSauce = await storage.read(key: 'userSauce');
      String configJson = await storage.read(key: 'config');
      bool passedAutofillOnboarding =
          (await storage.read(key: 'passedAutofillOnboarding')) == 'true';

      Config config;
      if (configJson != null) {
        try {
          Map configMap = jsonDecode(configJson);
          config = Config.fromJson(configMap);
        } on FormatException {
          // pass
        }
      }

      if (serverUrl == null || serverUrl == '') {
        if (config != null &&
            config.configJson != null &&
            config.configJson.backendServers != null &&
            config.configJson.backendServers.length > 0 &&
            config.configJson.backendServers[0].url != null) {
          serverUrl = config.configJson.backendServers[0].url;
        } else {
          final defaultHosts = getDefaultHosts();
          serverUrl = defaultHosts[0].url;
        }
      }

      Uint8List sessionSecretKeyBin;
      if (sessionSecretKey != null) {
        sessionSecretKeyBin = converter.fromHex(sessionSecretKey);
      }

      Uint8List secretKeyBin;
      if (secretKey != null) {
        secretKeyBin = converter.fromHex(secretKey);
      }

      Uint8List publicKeyBin;
      if (publicKey != null) {
        publicKeyBin = converter.fromHex(publicKey);
      }

      Uint8List privateKeyBin;
      if (privateKey != null) {
        privateKeyBin = converter.fromHex(privateKey);
      }

      reduxStore.dispatch(
        new InitiateStateAction(
          serverUrl,
          username,
          token,
          sessionSecretKeyBin,
          secretKeyBin,
          publicKeyBin,
          privateKeyBin,
          lockscreenPassphrase,
          lockscreenEnabled,
          complianceDisableDeleteAccount,
          userId,
          userEmail,
          userSauce,
          config,
        ),
      );

      bool autofillSupported = await autofillService.isSupported();

      await new Future.delayed(const Duration(milliseconds: 500));

      if (autofillSupported && !passedAutofillOnboarding) {
        Navigator.pushReplacementNamed(context, '/autofill_onboarding/');
      } else if (token == null) {
        Navigator.pushReplacementNamed(context, '/signin/');
      } else {
        if (lockscreenEnabled) {
          Navigator.pushReplacementNamed(context, '/passphrase/');
        } else {
          Navigator.pushReplacementNamed(context, '/datastore/');
        }
      }
    });

    return Scaffold(
      backgroundColor: Color(0xFF151f2b),
      body: component.Loading(hideLoadingIndicator: true),
    );
  }
}
