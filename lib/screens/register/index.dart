import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter/services.dart';
import 'package:flutter/gestures.dart';
import 'package:url_launcher/url_launcher.dart';

import 'package:psono/components/_index.dart' as component;
import 'package:psono/redux/store.dart';
import 'package:psono/services/api_client/index.dart' as apiClient;
import 'package:psono/services/storage.dart';
import 'package:psono/services/helper.dart' as helper;
import 'package:psono/services/manager_datastore_user.dart'
    as managerDatastoreUser;
import 'package:psono/redux/actions.dart';

class RegisterScreen extends StatefulWidget {
  static String tag = 'signin-screen';
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  final _usernameController = TextEditingController(
    text: '',
  );
  final _emailController = TextEditingController(
    text: '',
  );
  final _passwordController = TextEditingController(
    text: '',
  );
  final _passwordRepeatController = TextEditingController(
    text: '',
  );
  final _serverUrlController = TextEditingController(
    text: reduxStore.state.serverUrl,
  );

  String _screen = 'default';
  bool _obscurePassword = true;
  apiClient.Info info;

  String _domainSuffix = '@' + helper.getDomain(reduxStore.state.serverUrl);

  @override
  void dispose() {
    _usernameController?.dispose();
    _emailController?.dispose();
    _passwordController?.dispose();
    _passwordRepeatController?.dispose();
    _serverUrlController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final logo = Hero(
      tag: 'hero',
      child: CircleAvatar(
        backgroundColor: Colors.transparent,
        radius: 28.0,
        child: Image.asset('assets/images/logo.png'),
      ),
    );

    final username = TextFormField(
      controller: _usernameController,
      keyboardType: TextInputType.emailAddress,
      autofocus: true,
      decoration: InputDecoration(
        hintText: FlutterI18n.translate(context, "USERNAME"),
        filled: true,
        fillColor: Colors.white,
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(5)),
        suffixIcon: Padding(
          padding: const EdgeInsets.fromLTRB(20.0, 13.0, 20.0, 10.0),
          child: Text(
            _domainSuffix,
            style: TextStyle(color: Color(0xFFb1b6c1), fontSize: 16.0),
          ),
        ),
      ),
    );

    final email = TextFormField(
      controller: _emailController,
      autofocus: false,
      decoration: InputDecoration(
        hintText: FlutterI18n.translate(context, "EMAIL"),
        filled: true,
        fillColor: Colors.white,
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(5)),
      ),
    );

    final password = TextFormField(
      controller: _passwordController,
      autofocus: false,
      obscureText: _obscurePassword,
      decoration: InputDecoration(
        hintText: FlutterI18n.translate(context, "PASSWORD"),
        filled: true,
        fillColor: Colors.white,
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(5)),
        suffixIcon: IconButton(
          icon: Icon(
            _obscurePassword ? Icons.visibility : Icons.visibility_off,
            color: Theme.of(context).primaryColorDark,
          ),
          onPressed: () {
            setState(() {
              _obscurePassword = !_obscurePassword;
            });
          },
        ),
      ),
    );

    final passwordRepeat = TextFormField(
      controller: _passwordRepeatController,
      autofocus: false,
      obscureText: _obscurePassword,
      decoration: InputDecoration(
        hintText: FlutterI18n.translate(context, "PASSWORD_REPEAT"),
        filled: true,
        fillColor: Colors.white,
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(5)),
        suffixIcon: IconButton(
          icon: Icon(
            _obscurePassword ? Icons.visibility : Icons.visibility_off,
            color: Theme.of(context).primaryColorDark,
          ),
          onPressed: () {
            setState(() {
              _obscurePassword = !_obscurePassword;
            });
          },
        ),
      ),
    );

    void onChange() {
      if (_usernameController.text.contains('@')) {
        _domainSuffix = '';
      } else {
        _domainSuffix = '@' + helper.getDomain(_serverUrlController.text);
      }

      setState(() {
        _domainSuffix = _domainSuffix;
      });
    }

    _usernameController.addListener(onChange);
    _serverUrlController.addListener(onChange);

    void _showErrorDiaglog(String title, String content) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: new Text(FlutterI18n.translate(context, title)),
            content: new Text(FlutterI18n.translate(context, content)),
            actions: <Widget>[
              new FlatButton(
                child: new Text(FlutterI18n.translate(context, "CLOSE")),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }

    void initiateRegistration(
        String email, String username, String password) async {
      setState(() {
        _screen = 'loading';
      });
      try {
        await managerDatastoreUser.register(
          email,
          username,
          password,
          helper.getDomain(_serverUrlController.text),
        );
      } on apiClient.BadRequestException catch (e) {
        _showErrorDiaglog('ERROR', e.message);
        setState(() {
          _screen = 'default';
        });
        return;
      }
      setState(() {
        _screen = 'success';
      });
    }

    final registerButton = component.BtnPrimary(
      onPressed: () async {
        try {
          await storage.write(
            key: 'serverUrl',
            value: _serverUrlController.text,
            iOptions: secureIOSOptions,
          );
          await storage.write(
            key: 'username',
            value: _usernameController.text + _domainSuffix,
            iOptions: secureIOSOptions,
          );

          reduxStore.dispatch(
            new InitiateLoginAction(
              _serverUrlController.text,
              _usernameController.text + _domainSuffix,
            ),
          );
          info = await apiClient.info();
        } on apiClient.ServiceUnavailableException {
          _showErrorDiaglog(
            FlutterI18n.translate(context, "SERVER_OFFLINE"),
            FlutterI18n.translate(context, "SERVER_OFFLINE_DETAILS"),
          );
          return;
        } on HandshakeException {
          _showErrorDiaglog(
            FlutterI18n.translate(context, "HANDSHAKE_ERROR"),
            FlutterI18n.translate(context, "HANDSHAKE_ERROR_DETAILS"),
          );
          return;
        } catch (e) {
          _showErrorDiaglog(
            FlutterI18n.translate(context, "SERVER_OFFLINE"),
            FlutterI18n.translate(context, "SERVER_OFFLINE_DETAILS"),
          );
          return;
        }

        String testError = helper.isValidPassword(
          _passwordController.text,
          _passwordRepeatController.text,
          info.complianceMinMasterPasswordLength,
          info.complianceMinMasterPasswordComplexity,
        );
        if (testError != null) {
          _showErrorDiaglog(
            FlutterI18n.translate(context, "ERROR"),
            FlutterI18n.translate(context, testError),
          );
          return;
        }

        bool isValid = helper.isValidEmail(_emailController.text);
        if (!isValid) {
          _showErrorDiaglog(
            FlutterI18n.translate(context, "ERROR"),
            FlutterI18n.translate(context, 'INVALID_EMAIL_FORMAT'),
          );
          return;
        }

        String username = _usernameController.text + _domainSuffix;

        testError = helper.isValidUsername(username);
        if (testError != null) {
          _showErrorDiaglog(
            FlutterI18n.translate(context, "ERROR"),
            FlutterI18n.translate(context, testError),
          );
          return;
        }

        initiateRegistration(
            _emailController.text, username, _passwordController.text);
      },
      text: FlutterI18n.translate(context, "REGISTER"),
    );

    final server = TextFormField(
      keyboardType: TextInputType.url,
      controller: _serverUrlController,
      autofocus: false,
      decoration: InputDecoration(
        hintText: FlutterI18n.translate(context, "SERVER"),
        filled: true,
        fillColor: Colors.white,
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(5)),
      ),
    );

    if (_screen == 'loading') {
      return Scaffold(
        backgroundColor: Color(0xFF151f2b),
        body: component.Loading(),
      );
    } else if (_screen == 'success') {
      return Scaffold(
        backgroundColor: Color(0xFF151f2b),
        body: Center(
          child: ListView(
            shrinkWrap: true,
            padding: EdgeInsets.only(left: 24.0, right: 24.0),
            children: <Widget>[
              logo,
              SizedBox(height: 24.0),
              new Icon(
                component.FontAwesome.thumbs_o_up,
                color: Color(0xFFFFFFFF),
                size: 48.0,
              ),
              SizedBox(height: 24.0),
              component.AlertInfo(
                text: FlutterI18n.translate(context, "SUCCESSFUL_CHECK_EMAIL"),
              ),
              SizedBox(height: 24.0),
              component.BtnPrimary(
                text: FlutterI18n.translate(context, "BACK_TO_HOME"),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          ),
        ),
      );
    } else {
      return Scaffold(
        backgroundColor: Color(0xFF151f2b),
        body: Center(
          child: ListView(
            shrinkWrap: true,
            padding: EdgeInsets.only(left: 24.0, right: 24.0),
            children: <Widget>[
              logo,
              SizedBox(height: 24.0),
              username,
              SizedBox(height: 8.0),
              email,
              SizedBox(height: 8.0),
              password,
              SizedBox(height: 8.0),
              passwordRepeat,
              SizedBox(height: 8.0),
              Row(
                children: <Widget>[
                  Expanded(
                    child: registerButton,
                  ),
                  Expanded(
                    child: FlatButton(
                      child: new Text(FlutterI18n.translate(context, "ABORT")),
                      textColor: Color(0xFFb1b6c1),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ),
                ],
              ),
              SizedBox(height: 24.0),
              server,
              SizedBox(height: 16.0),
              new Center(
                child: new RichText(
                  text: new TextSpan(
                    children: [
                      new TextSpan(
                        text: FlutterI18n.translate(context, "PRIVACY_POLICY"),
                        style: new TextStyle(color: Color(0xFF666666)),
                        recognizer: new TapGestureRecognizer()
                          ..onTap = () async {
                            const url =
                                'https://www.psono.pw/privacy-policy.html';
                            if (await canLaunch(url)) {
                              await launch(url);
                            } else {
                              throw 'Could not launch $url';
                            }
                          },
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    }
  }
}
