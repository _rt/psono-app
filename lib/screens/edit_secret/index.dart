import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'package:psono/model/datastore.dart' as datastoreModel;
import 'package:psono/model/blueprint.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/model/secret.dart';
import 'package:psono/model/share.dart' as shareModel;
import 'package:psono/services/manager_datastore_password.dart'
    as managerDatastorePassword;
import 'package:psono/services/manager_secret.dart' as managerSecret;
import 'package:psono/services/manager_share.dart' as managerShare;
import 'package:psono/services/item_blueprint.dart' as itemBlueprint;

class EditSecretScreen extends StatefulWidget {
  static String tag = 'secret-screen';
  final datastoreModel.Folder parent;
  final datastoreModel.Datastore datastore;
  final datastoreModel.Folder share;
  final datastoreModel.Item item;
  final List<String> path;
  final List<String> relativePath;

  const EditSecretScreen({
    this.parent,
    this.datastore,
    this.share,
    this.item,
    this.path,
    this.relativePath,
  });

  @override
  _EditSecretScreenState createState() => _EditSecretScreenState();
}

class _EditSecretScreenState extends State<EditSecretScreen> {
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  bool _advanced = false;
  String _screen = 'loading';
  Secret _secret;
  Blueprint _bp;
  datastoreModel.Item item;
  List<TextEditingController> controllers = [];
  bool _obscurePassword = true;
  FToast _fToast;

  final logo = Hero(
    tag: 'hero',
    child: CircleAvatar(
      backgroundColor: Colors.transparent,
      radius: 28.0,
      child: Image.asset('assets/images/logo.png'),
    ),
  );

  Future<void> loadSecret() async {
    Secret secret = await managerSecret.readSecret(
        this.widget.item.secretId, this.widget.item.secretKey);
    setState(() {
      _secret = secret;
      _screen = 'default';
    });
  }

  _EditSecretScreenState({this.item});

  @override
  void initState() {
    super.initState();

    _fToast = FToast();
    _fToast.init(context);

    if (this.widget.item.secretId != null) {
      loadSecret();
      setState(() {
        _bp = itemBlueprint.getBlueprint(this.widget.item.type);
      });
    }
  }

  @override
  void dispose() {
    for (var i = 0; i < controllers.length; i++) {
      controllers[i]?.dispose();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Future _handleRefresh() async {
      await loadSecret();
    }

    int getFieldLength() {
      if (_advanced) {
        return _bp.fields.length + 1;
      } else {
        int advancedCount =
            _bp.fields.where((field) => field.position == 'advanced').length;
        return _bp.fields.length - advancedCount + 1;
      }
    }

    if (this.widget.item.secretId == null) {
      return Scaffold(
        backgroundColor: Color(0xFF151f2b),
        body: Center(
          child: ListView(
            shrinkWrap: true,
            padding: EdgeInsets.only(left: 24.0, right: 24.0),
            children: <Widget>[
              logo,
              SizedBox(height: 56.0),
              component.AlertInfo(
                text: FlutterI18n.translate(
                  context,
                  "ACCESS_DENIED",
                ),
              ),
              SizedBox(height: 8.0),
              Row(
                children: <Widget>[
                  component.BtnPrimary(
                    onPressed: () async {
                      Navigator.pop(context);
                    },
                    text: FlutterI18n.translate(context, "BACK"),
                  ),
                ],
              ),
            ],
          ),
        ),
      );
    }

    if (_screen == 'loading') {
      return Scaffold(
        backgroundColor: Color(0xFF151f2b),
        body: component.Loading(),
      );
    } else {
      return Scaffold(
        appBar: AppBar(
          title: Text(FlutterI18n.translate(context, "EDIT") +
              ' ' +
              FlutterI18n.translate(context, _bp.name)),
          elevation: 0,
          backgroundColor: Colors.white,
        ),
        backgroundColor: Color(0xFFebeeef),
        body: new RefreshIndicator(
          onRefresh: _handleRefresh,
          child: new Container(
            padding: new EdgeInsets.all(20.0),
            child: new Form(
              key: this._formKey,
              child: ListView.builder(
                // Let the ListView know how many items it needs to build.
                itemCount: getFieldLength(),
                // Provide a builder function. This is where the magic happens.
                // Convert each item into a widget based on the type of item it is.
                itemBuilder: (context, index) {
                  if (index == getFieldLength() - 1) {
                    // last element, render the save button

                    return Padding(
                      padding: const EdgeInsets.symmetric(vertical: 16.0),
                      child: component.BtnSuccess(
                        onPressed: () async {
                          if (!_formKey.currentState.validate()) {
                            return;
                          }
                          setState(() {
                            _screen = 'loading';
                          });

                          _secret.save();

                          List<String> itemPath =
                              new List<String>.from(this.widget.relativePath);
                          itemPath.add(this.widget.item.id);

                          if (this.widget.item.shareId != null) {
                            shareModel.Share share =
                                await managerShare.readShare(
                              this.widget.item.shareId,
                              this.widget.item.shareSecretKey,
                            );

                            share.item.name = _secret.data[_bp.titleField];
                            if (_bp.urlfilterField != null) {
                              share.item.urlfilter =
                                  _secret.data[_bp.urlfilterField];
                            }

                            this.widget.item.name =
                                _secret.data[_bp.titleField];
                            if (_bp.urlfilterField != null) {
                              this.widget.item.urlfilter =
                                  _secret.data[_bp.urlfilterField];
                            }

                            await share.save();
                          } else if (this.widget.share == null) {
                            datastoreModel.Datastore datastore =
                                await managerDatastorePassword
                                    .getPasswordDatastore(
                              this.widget.datastore.datastoreId,
                            );

                            List<String> pathCopy =
                                new List<String>.from(itemPath);
                            List search =
                                managerDatastorePassword.findInDatastore(
                              pathCopy,
                              datastore.data,
                            );
                            datastoreModel.Item remoteItem =
                                search[0][search[1]];

                            remoteItem.name = _secret.data[_bp.titleField];
                            if (_bp.urlfilterField != null) {
                              remoteItem.urlfilter =
                                  _secret.data[_bp.urlfilterField];
                            }

                            this.widget.item.name =
                                _secret.data[_bp.titleField];
                            if (_bp.urlfilterField != null) {
                              this.widget.item.urlfilter =
                                  _secret.data[_bp.urlfilterField];
                            }

                            await datastore.save();
                          } else {
                            shareModel.Share share =
                                await managerShare.readShare(
                              this.widget.share.shareId,
                              this.widget.share.shareSecretKey,
                            );

                            List<String> pathCopy =
                                new List<String>.from(itemPath);
                            List search =
                                managerDatastorePassword.findInDatastore(
                              pathCopy,
                              share.folder,
                            );
                            datastoreModel.Item remoteItem =
                                search[0][search[1]];

                            remoteItem.name = _secret.data[_bp.titleField];
                            if (_bp.urlfilterField != null) {
                              remoteItem.urlfilter =
                                  _secret.data[_bp.urlfilterField];
                            }

                            this.widget.item.name =
                                _secret.data[_bp.titleField];
                            if (_bp.urlfilterField != null) {
                              this.widget.item.urlfilter =
                                  _secret.data[_bp.urlfilterField];
                            }

                            await share.save();
                          }
                          setState(() {
                            _screen = 'default';
                          });
                          return;
                        },
                        text: FlutterI18n.translate(context, "SAVE"),
                      ),
                    );
                  } else {
                    final field = _bp.fields[index];

                    String content = '';

                    if (_secret.data.containsKey(field.name)) {
                      content = _secret.data[field.name];
                    }

                    TextInputType keyboardType;
                    bool obscureText = false;
                    int maxLines = 1;

                    List<DropdownMenuItem<String>> buttons = [];

                    if (field.field == 'input' && field.type == 'text') {
                    } else if (field.field == 'input' &&
                        field.type == 'password') {
                      obscureText = true && _obscurePassword;
                      buttons.add(DropdownMenuItem<String>(
                        value: "show-hide",
                        child: Row(children: <Widget>[
                          Icon(
                            _obscurePassword
                                ? Icons.visibility
                                : Icons.visibility_off,
                            color: Theme.of(context).primaryColorDark,
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 12.0),
                            child: Text(_obscurePassword
                                ? FlutterI18n.translate(
                                    context, "SHOW_PASSWORD")
                                : FlutterI18n.translate(
                                    context, "HIDE_PASSWORD")),
                          ),
                        ]),
                      ));
                      buttons.add(DropdownMenuItem<String>(
                        value: "generate",
                        child: Row(children: <Widget>[
                          Icon(
                            component.FontAwesome.cogs,
                            color: Theme.of(context).primaryColorDark,
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 12.0),
                            child: Text(FlutterI18n.translate(
                                context, "GENERATE_PASSWORD")),
                          ),
                        ]),
                      ));
                    } else if (field.field == 'input' &&
                        field.type == 'totp_code') {
                      obscureText = true && _obscurePassword;
                      buttons.add(DropdownMenuItem<String>(
                        value: "show-hide",
                        child: Row(children: <Widget>[
                          Icon(
                            _obscurePassword
                                ? Icons.visibility
                                : Icons.visibility_off,
                            color: Theme.of(context).primaryColorDark,
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 12.0),
                            child: Text(_obscurePassword
                                ? FlutterI18n.translate(context, "SHOW")
                                : FlutterI18n.translate(context, "HIDE")),
                          ),
                        ]),
                      ));
                    } else if (field.field == 'input' &&
                        field.type == 'checkbox') {
                    } else if (field.field == 'textarea') {
                      keyboardType = TextInputType.multiline;
                      maxLines = null;
                    } else if (field.field == 'button' &&
                        field.type == 'button') {
                      keyboardType = TextInputType.multiline;
                    } else {
                      throw ("unknown field type combi");
                    }

                    final textController = TextEditingController(
                      text: content,
                    );

                    controllers.add(textController);
                    buttons.add(DropdownMenuItem<String>(
                      value: "copy-to-clipboard",
                      child: Row(children: <Widget>[
                        Icon(
                          component.FontAwesome.clipboard,
                          color: Theme.of(context).primaryColorDark,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 12.0),
                          child: Text(field.field == 'input' &&
                                  field.type == 'password'
                              ? FlutterI18n.translate(context, "COPY_PASSWORD")
                              : FlutterI18n.translate(context, "COPY")),
                        ),
                      ]),
                    ));

                    List<Widget> children = [];
                    if (field.field == 'input' && field.type == 'totp_code') {
                      children.add(
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 10.0),
                          child: component.TotpCode(
                            code: content,
                          ),
                        ),
                      );
                    } else {
                      children.add(
                        Stack(
                          alignment: Alignment.centerRight,
                          children: [
                            TextFormField(
                              obscureText: obscureText,
                              controller: textController,
                              validator: (value) {
                                if (field.required && value.isEmpty) {
                                  return FlutterI18n.translate(
                                    context,
                                    field.errorMessageRequired,
                                  );
                                }
                                return null;
                              },
                              keyboardType: keyboardType,
                              maxLines: maxLines,
                              onChanged: (text) {
                                _secret.data[field.name] = text;
                              },
                              decoration: new InputDecoration(
                                  contentPadding:
                                      EdgeInsets.fromLTRB(0, 10, 48.0, 10),
                                  labelText: FlutterI18n.translate(
                                      context, field.title)),
                            ),
                            new DropdownButtonHideUnderline(
                              child: DropdownButton<String>(
                                icon: Icon(component.FontAwesome.ellipsis_v),
                                iconSize: 20,
                                dropdownColor: Colors.white,
                                onChanged: (String newValue) async {
                                  if (newValue == 'copy-to-clipboard') {
                                    Clipboard.setData(
                                      new ClipboardData(
                                        text: textController.text,
                                      ),
                                    );
                                    _clipboardCopyToast();
                                  }
                                  if (newValue == 'show-hide') {
                                    setState(() {
                                      _obscurePassword = !_obscurePassword;
                                    });
                                  }
                                  if (newValue == 'generate') {
                                    textController.text =
                                        await managerDatastorePassword
                                            .generate();
                                    _secret.data[field.name] =
                                        textController.text;
                                  }
                                },
                                items: buttons,
                              ),
                            )
                          ],
                        ),
                      );
                    }

                    return new Container(
                      child: new Column(
                        children: children,
                      ),
                    );
                  }
                },
              ),
            ),
          ),
        ),
      );
    }
  }

  _clipboardCopyToast() {
    Widget toast = Container(
      padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(25.0),
        color: Color(0xFF5cb85c),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(
            component.FontAwesome.clipboard,
            color: Colors.white,
          ),
          SizedBox(
            width: 12.0,
          ),
          Text(
            FlutterI18n.translate(context, "CLIPBOARD_COPY"),
            style: TextStyle(color: Colors.white),
          ),
        ],
      ),
    );

    _fToast.showToast(
        child: toast,
        toastDuration: Duration(seconds: 2),
        positionedToastBuilder: (context, child) {
          return Positioned(child: child, top: 110, left: 0, right: 0);
        });
  }
}
