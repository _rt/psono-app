import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:psono/screens/edit_folder/index.dart';

import 'package:psono/theme.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/model/datastore.dart' as datastoreModel;
import 'package:psono/screens/custom_floating_action_button.dart';
import 'package:psono/screens/datastore/index.dart';
import 'package:psono/screens/edit_secret/index.dart';
import 'package:psono/services/manager_widget.dart' as managerWidget;

class FolderScreen extends StatefulWidget {
  static String tag = 'folder-screen';
  final List<String> autoNavigate;
  final datastoreModel.Folder folder;
  final datastoreModel.Datastore datastore;
  final datastoreModel.Folder share;
  final List<String> path;
  final List<String> relativePath;
  final bool autofill;

  const FolderScreen({
    this.autoNavigate,
    this.folder,
    this.datastore,
    this.share,
    this.path,
    this.relativePath,
    this.autofill,
  });

  @override
  _FolderScreenState createState() => _FolderScreenState();
}

class _FolderScreenState extends State<FolderScreen> {
  String _search;
  List<String> selectedItems = [];
  List<String> selectedFolders = [];
  bool alreadyLoadedOnce = false;

  Future<void> loadDatastore() async {
    Navigator.pushReplacement(
      context,
      NoAnimationMaterialPageRoute(
        builder: (context) => DatastoreScreen(
          autoNavigate: this.widget.path,
        ),
      ),
    );
  }

  Future<void> initStateAsync() async {
    if (this.widget.autoNavigate != null &&
        this.widget.autoNavigate.length > 0) {
      List<String> newAutoNavigate = List.from(this.widget.autoNavigate);
      String folderId = newAutoNavigate.removeAt(0);

      for (var i = 0; i < this.widget.folder.folders.length; i++) {
        datastoreModel.Folder folder = this.widget.folder.folders[i];
        if (folder.id != folderId) {
          continue;
        }
        datastoreModel.Folder newShare;
        List<String> newRelativePath;
        if (folder.shareId != null) {
          newShare = folder;
          newRelativePath = [];
        } else {
          newRelativePath = List.from(this.widget.relativePath)
            ..addAll([folder.id]);
        }

        Navigator.push(
          context,
          NoAnimationMaterialPageRoute(
            builder: (context) => FolderScreen(
              autoNavigate: newAutoNavigate,
              folder: folder,
              datastore: this.widget.datastore,
              share: newShare,
              path: List.from(this.widget.path)..addAll([folder.id]),
              relativePath: newRelativePath,
            ),
          ),
        );
        return;
      }
    }
  }

  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) async {
      initStateAsync();
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Future _handleRefresh() async {
      await loadDatastore();
    }

    void searchCallback(String search) {
      setState(() {
        _search = search;
      });
    }

    void deleteCallback() async {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: new Text(FlutterI18n.translate(context, 'DELETE_ENTRY')),
            content: new Text(
                FlutterI18n.translate(context, 'DELETE_ENTRY_WARNING')),
            actions: <Widget>[
              new FlatButton(
                child: new Text(FlutterI18n.translate(context, "ABORT")),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              new component.BtnPrimary(
                onPressed: () async {
                  await managerWidget.deleteItem(
                    selectedItems,
                    selectedFolders,
                    this.widget.relativePath,
                    this.widget.share,
                    this.widget.datastore,
                    'password',
                  );

                  setState(() {
                    selectedItems = [];
                    selectedFolders = [];
                  });

                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                      builder: (context) => DatastoreScreen(
                        autoNavigate: this.widget.path,
                      ),
                    ),
                  );
                },
                text: FlutterI18n.translate(context, "YES"),
              ),
            ],
          );
        },
      );
    }

    void editCallback() async {
      if (selectedItems.length == 1) {
        // Trigger edit of an item
        datastoreModel.Item item = this
            .widget
            .folder
            .items
            .where((item) => selectedItems.contains(item.id))
            .first;
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => EditSecretScreen(
              parent: this.widget.folder,
              datastore: this.widget.datastore,
              share: this.widget.share,
              item: item,
              path: this.widget.path,
              relativePath: this.widget.relativePath,
            ),
          ),
        );
      } else if (selectedFolders.length == 1) {
        // Trigger edit of a folder
        datastoreModel.Folder folder = this
            .widget
            .folder
            .folders
            .where((folder) => selectedFolders.contains(folder.id))
            .first;
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => EditFolderScreen(
              parent: this.widget.folder,
              datastore: this.widget.datastore,
              share: this.widget.share,
              folder: folder,
              path: this.widget.path,
              relativePath: this.widget.relativePath,
            ),
          ),
        );
      }
    }

    Widget floatingActionButton;

    Widget contentView;
    if (_search == null) {
      if (selectedItems.length == 0 && selectedFolders.length == 0) {
        contentView = component.FolderTree(
          root: this.widget.folder,
          datastore: this.widget.datastore,
          share: this.widget.share,
          path: this.widget.path,
          relativePath: this.widget.relativePath,
          autofill: this.widget.autofill,
          onLongPressFolder: (datastoreModel.Folder folder) async {
            if (!folder.shareRights.delete) {
              return;
            }
            setState(() {
              selectedItems = [];
              selectedFolders = [folder.id];
            });
          },
          onLongPressItem: (datastoreModel.Item item) async {
            if (!item.shareRights.delete) {
              return;
            }
            setState(() {
              selectedItems = [item.id];
              selectedFolders = [];
            });
          },
        );

        floatingActionButton = CustomFloatingActionButton(
          parent: this.widget.folder,
          datastore: this.widget.datastore,
          share: this.widget.share,
          path: this.widget.path,
          relativePath: this.widget.relativePath,
          onRefresh: _handleRefresh,
        );
      } else {
        contentView = component.FolderSelectTree(
          root: this.widget.folder,
          datastore: this.widget.datastore,
          share: this.widget.share,
          path: this.widget.path,
          relativePath: this.widget.relativePath,
          selectedFolders: selectedFolders,
          selectedItems: selectedItems,
          onSelectFolder: (datastoreModel.Folder folder) async {
            setState(() {
              if (selectedFolders.contains(folder.id)) {
                selectedFolders.remove(folder.id);
              } else {
                if (!folder.shareRights.delete) {
                  return;
                }
                selectedFolders.add(folder.id);
              }
            });
          },
          onSelectItem: (datastoreModel.Item item) async {
            setState(() {
              if (selectedItems.contains(item.id)) {
                selectedItems.remove(item.id);
              } else {
                if (!item.shareRights.delete) {
                  return;
                }
                selectedItems.add(item.id);
              }
            });
          },
        );
      }
    } else {
      contentView = component.FolderSearchTree(
        root: this.widget.folder,
        datastore: this.widget.datastore,
        share: this.widget.share,
        path: this.widget.path,
        relativePath: this.widget.relativePath,
        search: _search,
        autofill: this.widget.autofill,
      );
    }

    Widget appBar;
    if (selectedItems.length + selectedFolders.length > 0) {
      appBar = component.SliverAppMultiselectBar(
        onDelete: deleteCallback,
        onEdit: editCallback,
        selectedElements: selectedItems.length + selectedFolders.length,
      );
    } else {
      appBar = component.SliverAppSearchBar(
        title: this.widget.folder.name,
        onSearch: searchCallback,
      );
    }

    return Scaffold(
      backgroundColor: Color(0xFFebeeef),
      body: new RefreshIndicator(
        onRefresh: _handleRefresh,
        child: CustomScrollView(
          slivers: <Widget>[
            appBar,
            contentView,
          ],
        ),
      ),
      floatingActionButton: floatingActionButton,
    );
  }
}
