import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter/gestures.dart';
import 'package:url_launcher/url_launcher.dart';

import 'package:psono/components/_index.dart' as component;
import 'package:psono/services/storage.dart';

class AutofillOnboardingIOS extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: ListView(
        shrinkWrap: true,
        padding: EdgeInsets.only(left: 24.0, right: 24.0),
        children: <Widget>[
          SizedBox(height: 24.0),
          Text(
            FlutterI18n.translate(context, "AUTOFILL_SERVICE"),
            textAlign: TextAlign.left,
            style: TextStyle(color: Color(0xFFb1b6c1)),
          ),
          SizedBox(height: 16.0),
          component.AlertInfo(
            text: FlutterI18n.translate(
              context,
              "AUTOFILL_SERVICE_ACTIVATE_NOW_INFO_IOS",
            ),
          ),
          ListTile(
            title: Text(
              FlutterI18n.translate(context, 'OPEN_SETTINGS_APP'),
              style: TextStyle(
                color: Color(0xFFFFFFFF),
              ),
            ),
            leading: Image(
              image: AssetImage("assets/images/iconSettings.png"),
              height: 30.0,
              width: 30.0,
              fit: BoxFit.fitWidth,
            ),
          ),
          ListTile(
            title: Text(
              FlutterI18n.translate(context, 'TAP_INTO_PASSWORDS_AND_ACCOUNTS'),
              style: TextStyle(
                color: Color(0xFFFFFFFF),
              ),
            ),
            leading: Image(
              image: AssetImage("assets/images/iconPasswordsAccounts.png"),
              height: 30.0,
              width: 30.0,
              fit: BoxFit.fitWidth,
            ),
          ),
          ListTile(
            title: Text(
              FlutterI18n.translate(context, 'TAP_INTO_AUTOFILL_PASSWORDSS'),
              style: TextStyle(
                color: Color(0xFFFFFFFF),
              ),
            ),
            leading: Image(
              image: AssetImage("assets/images/iconAutofill.png"),
              height: 30.0,
              width: 30.0,
              fit: BoxFit.fitWidth,
            ),
          ),
          ListTile(
            title: Text(
              FlutterI18n.translate(context, 'TURN_ON_AUTOFILL_PASSWORDS'),
              style: TextStyle(
                color: Color(0xFFFFFFFF),
              ),
            ),
            leading: Image(
              image: AssetImage("assets/images/iconOn.png"),
              height: 30.0,
              width: 30.0,
              fit: BoxFit.fitWidth,
            ),
          ),
          ListTile(
            title: Text(
              FlutterI18n.translate(context, 'SELECT_PSONO'),
              style: TextStyle(
                color: Color(0xFFFFFFFF),
              ),
            ),
            leading: Image(
              image: AssetImage("assets/images/iconPsonoApp.png"),
              height: 30.0,
              width: 30.0,
              fit: BoxFit.fitWidth,
            ),
          ),
          ListTile(
            title: Text(
              FlutterI18n.translate(context, 'DESELECT_KEYCHAIN'),
              style: TextStyle(
                color: Color(0xFFFFFFFF),
              ),
            ),
            leading: Image(
              image: AssetImage("assets/images/iconPasswordsAccounts.png"),
              height: 30.0,
              width: 30.0,
              fit: BoxFit.fitWidth,
            ),
          ),
          SizedBox(height: 24.0),
          Row(
            children: <Widget>[
              Expanded(
                child: component.BtnPrimary(
                  onPressed: () async {
                    await storage.write(
                      key: 'passedAutofillOnboarding',
                      value: 'true',
                      iOptions: secureIOSOptions,
                    );
                    Navigator.pushReplacementNamed(context, '/');
                  },
                  text: FlutterI18n.translate(context, "OK"),
                ),
              ),
            ],
          ),
          SizedBox(height: 16.0),
          new Center(
            child: new RichText(
              text: new TextSpan(
                children: [
                  new TextSpan(
                    text: FlutterI18n.translate(context, "PRIVACY_POLICY"),
                    style: new TextStyle(color: Color(0xFF666666)),
                    recognizer: new TapGestureRecognizer()
                      ..onTap = () async {
                        const url = 'https://www.psono.pw/privacy-policy.html';
                        if (await canLaunch(url)) {
                          await launch(url);
                        } else {
                          throw 'Could not launch $url';
                        }
                      },
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
