import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter/gestures.dart';
import 'package:url_launcher/url_launcher.dart';

import 'package:psono/components/_index.dart' as component;
import 'package:psono/services/autofill.dart' as autofillService;
import 'package:psono/services/storage.dart';

class AutofillOnboardingAndroid extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final logo = Hero(
      tag: 'hero',
      child: CircleAvatar(
        backgroundColor: Colors.transparent,
        radius: 28.0,
        child: Image.asset('assets/images/logo.png'),
      ),
    );

    return Center(
      child: ListView(
        shrinkWrap: true,
        padding: EdgeInsets.only(left: 24.0, right: 24.0),
        children: <Widget>[
          logo,
          SizedBox(height: 24.0),
          Text(
            FlutterI18n.translate(context, "AUTOFILL_SERVICE"),
            textAlign: TextAlign.left,
            style: TextStyle(color: Color(0xFFb1b6c1)),
          ),
          SizedBox(height: 16.0),
          component.AlertInfo(
            text: FlutterI18n.translate(
              context,
              "AUTOFILL_SERVICE_ACTIVATE_NOW_INFO",
            ),
          ),
          SizedBox(height: 24.0),
          Row(
            children: <Widget>[
              Expanded(
                child: component.BtnPrimary(
                  onPressed: () async {
                    await storage.write(
                      key: 'passedAutofillOnboarding',
                      value: 'true',
                      iOptions: secureIOSOptions,
                    );
                    await autofillService.enable();
                    Navigator.pushReplacementNamed(context, '/');
                  },
                  text: FlutterI18n.translate(context, "ACTIVATE"),
                ),
              ),
              Expanded(
                child: FlatButton(
                  child: new Text(FlutterI18n.translate(context, "SKIP")),
                  textColor: Color(0xFFb1b6c1),
                  onPressed: () async {
                    await storage.write(
                      key: 'passedAutofillOnboarding',
                      value: 'true',
                      iOptions: secureIOSOptions,
                    );

                    Navigator.pushReplacementNamed(context, '/');
                  },
                ),
              ),
            ],
          ),
          SizedBox(height: 16.0),
          new Center(
            child: new RichText(
              text: new TextSpan(
                children: [
                  new TextSpan(
                    text: FlutterI18n.translate(context, "PRIVACY_POLICY"),
                    style: new TextStyle(color: Color(0xFF666666)),
                    recognizer: new TapGestureRecognizer()
                      ..onTap = () async {
                        const url = 'https://www.psono.pw/privacy-policy.html';
                        if (await canLaunch(url)) {
                          await launch(url);
                        } else {
                          throw 'Could not launch $url';
                        }
                      },
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
