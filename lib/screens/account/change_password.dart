import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/services/api_client/index.dart' as apiClient;
import 'package:psono/services/manager_datastore_user.dart'
    as managerDatastoreUser;
import 'package:psono/services/helper.dart' as helper;

class AccountChangePasswordScreen extends StatefulWidget {
  static String tag = 'account-change-password-screen';

  @override
  _AccountChangePasswordScreenState createState() =>
      _AccountChangePasswordScreenState();
}

class _AccountChangePasswordScreenState
    extends State<AccountChangePasswordScreen> {
  final newPassword = TextEditingController(
    text: '',
  );
  final newPasswordRepeat = TextEditingController(
    text: '',
  );
  final oldPassword = TextEditingController(
    text: '',
  );

  String _screen = 'default';
  String message;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    newPassword?.dispose();
    newPasswordRepeat?.dispose();
    oldPassword?.dispose();
    super.dispose();
  }

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    Widget messageWidget = Container();
    apiClient.Info info;
    if (message != null) {
      messageWidget = component.AlertInfo(
        text: message,
      );
    }

    void _showErrorDiaglog(String title, String content) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: new Text(FlutterI18n.translate(context, title)),
            content: new Text(FlutterI18n.translate(context, content)),
            actions: <Widget>[
              new FlatButton(
                child: new Text(FlutterI18n.translate(context, "CLOSE")),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }

    if (_screen == 'loading') {
      return Scaffold(
        backgroundColor: Color(0xFF151f2b),
        body: component.Loading(),
      );
    } else {
      return Scaffold(
        appBar: AppBar(
          title: Text(FlutterI18n.translate(
            context,
            'CHANGE_PASSWORD',
          )),
          elevation: 0,
          backgroundColor: Colors.white,
        ),
        backgroundColor: Color(0xFFebeeef),
        body: new Container(
          padding: new EdgeInsets.all(20.0),
          child: Form(
            key: _formKey,
            child: ListView(
              children: <Widget>[
                component.AlertInfo(
                  text: FlutterI18n.translate(
                      context, "CHANGE_PASSWORD_DESCRIPTION"),
                ),
                new TextFormField(
                  obscureText: true,
                  controller: newPassword,
                  autofocus: true,
                  validator: (value) {
                    String testFailure = helper.isValidPassword(
                      value,
                      newPasswordRepeat.text,
                      info.complianceMinMasterPasswordLength,
                      info.complianceMinMasterPasswordComplexity,
                    );
                    if (testFailure == null) {
                      return null;
                    }
                    return FlutterI18n.translate(
                      context,
                      testFailure,
                    );
                  },
                  decoration: new InputDecoration(
                    labelText: FlutterI18n.translate(
                      context,
                      'NEW_PASSWORD',
                    ),
                  ),
                ),
                new TextFormField(
                  obscureText: true,
                  controller: newPasswordRepeat,
                  autofocus: true,
                  validator: (value) {
                    if (value.isNotEmpty && value != newPassword.text) {
                      return FlutterI18n.translate(
                        context,
                        'PASSWORDS_DONT_MATCH',
                      );
                    }
                    return null;
                  },
                  decoration: new InputDecoration(
                    labelText: FlutterI18n.translate(
                      context,
                      'NEW_PASSWORD_REPEAT',
                    ),
                  ),
                ),
                new TextFormField(
                  obscureText: true,
                  controller: oldPassword,
                  validator: (value) {
                    if (value.isEmpty) {
                      return FlutterI18n.translate(
                        context,
                        'OLD_PASSWORD_REQUIRED',
                      );
                    }
                    return null;
                  },
                  decoration: new InputDecoration(
                    labelText: FlutterI18n.translate(
                      context,
                      'OLD_PASSWORD',
                    ),
                  ),
                ),
                SizedBox(height: 16.0),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: component.BtnSuccess(
                        onPressed: () async {
                          try {
                            info = await apiClient.info();
                          } on apiClient.ServiceUnavailableException {
                            _showErrorDiaglog(
                              FlutterI18n.translate(context, "SERVER_OFFLINE"),
                              FlutterI18n.translate(
                                  context, "SERVER_OFFLINE_DETAILS"),
                            );
                            return;
                          } on HandshakeException {
                            _showErrorDiaglog(
                              FlutterI18n.translate(context, "HANDSHAKE_ERROR"),
                              FlutterI18n.translate(
                                  context, "HANDSHAKE_ERROR_DETAILS"),
                            );
                            return;
                          } catch (e) {
                            _showErrorDiaglog(
                              FlutterI18n.translate(context, "SERVER_OFFLINE"),
                              FlutterI18n.translate(
                                  context, "SERVER_OFFLINE_DETAILS"),
                            );
                            return;
                          }

                          if (!_formKey.currentState.validate()) {
                            return;
                          }

                          setState(() {
                            _screen = 'loading';
                            message = null;
                          });
                          try {
                            await managerDatastoreUser.saveNewPassword(
                              newPassword.text,
                              newPasswordRepeat.text,
                              oldPassword.text,
                            );
                          } catch (e) {
                            setState(() {
                              _screen = 'default';
                            });
                            _showErrorDiaglog('ERROR', e.message);
                            return;
                          }

                          newPassword.text = '';
                          newPasswordRepeat.text = '';
                          oldPassword.text = '';

                          setState(() {
                            _screen = 'default';
                            message =
                                FlutterI18n.translate(context, "SAVE_SUCCESS");
                          });
                        },
                        text: FlutterI18n.translate(context, "SAVE"),
                      ),
                    ),
                    Expanded(
                      child: component.Btn(
                        onPressed: () async {
                          Navigator.pop(context);
                        },
                        text: FlutterI18n.translate(context, "BACK"),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 16.0),
                messageWidget,
              ],
            ),
          ),
        ),
      );
    }
  }
}
