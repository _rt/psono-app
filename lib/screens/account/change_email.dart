import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:psono/redux/store.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/services/manager_datastore_user.dart'
    as managerDatastoreUser;

class AccountChangeEmailScreen extends StatefulWidget {
  static String tag = 'account-change-email-screen';

  @override
  _AccountChangeEmailScreenState createState() =>
      _AccountChangeEmailScreenState();
}

class _AccountChangeEmailScreenState extends State<AccountChangeEmailScreen> {
  final email = TextEditingController(
    text: reduxStore.state.userEmail,
  );
  final password = TextEditingController(
    text: '',
  );

  String _screen = 'default';
  String message;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    email?.dispose();
    password?.dispose();
    super.dispose();
  }

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    Widget messageWidget = Container();
    if (message != null) {
      messageWidget = component.AlertInfo(
        text: message,
      );
    }

    void _showErrorDiaglog(String title, String content) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: new Text(FlutterI18n.translate(context, title)),
            content: new Text(FlutterI18n.translate(context, content)),
            actions: <Widget>[
              new FlatButton(
                child: new Text(FlutterI18n.translate(context, "CLOSE")),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }

    if (_screen == 'loading') {
      return Scaffold(
        backgroundColor: Color(0xFF151f2b),
        body: component.Loading(),
      );
    } else {
      return Scaffold(
        appBar: AppBar(
          title: Text(FlutterI18n.translate(
            context,
            'CHANGE_E_MAIL',
          )),
          elevation: 0,
          backgroundColor: Colors.white,
        ),
        backgroundColor: Color(0xFFebeeef),
        body: new Container(
          padding: new EdgeInsets.all(20.0),
          child: Form(
            key: _formKey,
            child: ListView(
              children: <Widget>[
                component.AlertInfo(
                  text: FlutterI18n.translate(
                      context, "CHANGE_E_MAIL_DESCRIPTION"),
                ),
                new TextFormField(
                  controller: email,
                  keyboardType: TextInputType.emailAddress,
                  autofocus: true,
                  validator: (value) {
                    if (value.isEmpty) {
                      return FlutterI18n.translate(
                        context,
                        'INVALID_EMAIL_IN_EMAIL',
                      );
                    }
                    return null;
                  },
                  decoration: new InputDecoration(
                    labelText: FlutterI18n.translate(
                      context,
                      'NEW_E_MAIL',
                    ),
                  ),
                ),
                new TextFormField(
                  obscureText: true,
                  controller: password,
                  validator: (value) {
                    if (value.isEmpty) {
                      return FlutterI18n.translate(
                        context,
                        'OLD_PASSWORD_REQUIRED',
                      );
                    }
                    return null;
                  },
                  decoration: new InputDecoration(
                    labelText: FlutterI18n.translate(
                      context,
                      'CURRENT_PASSWORD',
                    ),
                  ),
                ),
                SizedBox(height: 16.0),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: component.BtnSuccess(
                        onPressed: () async {
                          if (!_formKey.currentState.validate()) {
                            return;
                          }
                          setState(() {
                            _screen = 'loading';
                            message = null;
                          });
                          try {
                            await managerDatastoreUser.saveNewEmail(
                              email.text,
                              password.text,
                            );
                          } catch (e) {
                            setState(() {
                              _screen = 'default';
                            });
                            _showErrorDiaglog('ERROR', e.message);
                            return;
                          }

                          password.text = '';

                          setState(() {
                            _screen = 'default';
                            message =
                                FlutterI18n.translate(context, "SAVE_SUCCESS");
                          });
                        },
                        text: FlutterI18n.translate(context, "SAVE"),
                      ),
                    ),
                    Expanded(
                      child: component.Btn(
                        onPressed: () async {
                          Navigator.pop(context);
                        },
                        text: FlutterI18n.translate(context, "BACK"),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 16.0),
                messageWidget,
              ],
            ),
          ),
        ),
      );
    }
  }
}
