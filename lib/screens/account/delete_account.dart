import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/services/manager_datastore_user.dart'
    as managerDatastoreUser;

class AccountDeleteAccountScreen extends StatefulWidget {
  static String tag = 'account-delete-account-screen';

  @override
  _AccountDeleteAccountScreenState createState() =>
      _AccountDeleteAccountScreenState();
}

class _AccountDeleteAccountScreenState
    extends State<AccountDeleteAccountScreen> {
  final password = TextEditingController(
    text: '',
  );

  String _screen = 'default';

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    password?.dispose();
    super.dispose();
  }

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    void _showErrorDiaglog(String title, String content) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: new Text(FlutterI18n.translate(context, title)),
            content: new Text(FlutterI18n.translate(context, content)),
            actions: <Widget>[
              new FlatButton(
                child: new Text(FlutterI18n.translate(context, "CLOSE")),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }

    if (_screen == 'loading') {
      return Scaffold(
        backgroundColor: Color(0xFF151f2b),
        body: component.Loading(),
      );
    } else {
      return Scaffold(
        appBar: AppBar(
          title: Text(FlutterI18n.translate(
            context,
            'DELETE_ACCOUNT',
          )),
          elevation: 0,
          backgroundColor: Colors.white,
        ),
        backgroundColor: Color(0xFFebeeef),
        body: new Container(
          padding: new EdgeInsets.all(20.0),
          child: Form(
            key: _formKey,
            child: ListView(
              children: <Widget>[
                component.AlertDanger(
                  text: FlutterI18n.translate(
                      context, "YOU_ARE_ABOUT_TO_DELETE_YOUR_ACCOUNT"),
                ),
                new TextFormField(
                  obscureText: true,
                  controller: password,
                  validator: (value) {
                    if (value.isEmpty) {
                      return FlutterI18n.translate(
                        context,
                        'OLD_PASSWORD_REQUIRED',
                      );
                    }
                    return null;
                  },
                  decoration: new InputDecoration(
                    labelText: FlutterI18n.translate(
                      context,
                      'YOUR_PASSWORD_AS_CONFIRMATION',
                    ),
                  ),
                ),
                SizedBox(height: 16.0),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: component.BtnDanger(
                        onPressed: () async {
                          if (!_formKey.currentState.validate()) {
                            return;
                          }
                          setState(() {
                            _screen = 'loading';
                          });
                          try {
                            await managerDatastoreUser
                                .deleteAccount(password.text);
                          } catch (e) {
                            setState(() {
                              _screen = 'default';
                            });
                            _showErrorDiaglog('ERROR', e.message);
                            return;
                          }
                          Navigator.pushReplacementNamed(context, '/signin/');
                        },
                        text: FlutterI18n.translate(context, "DELETE"),
                      ),
                    ),
                    Expanded(
                      child: component.Btn(
                        onPressed: () async {
                          Navigator.pop(context);
                        },
                        text: FlutterI18n.translate(context, "BACK"),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      );
    }
  }
}
