import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'package:psono/model/datastore.dart' as datastoreModel;
import 'package:psono/model/share.dart' as shareModel;
import 'package:psono/services/manager_datastore_password.dart'
    as managerDatastorePassword;
import 'package:psono/services/manager_share.dart' as managerShare;
import 'package:psono/components/_index.dart' as component;
import 'package:psono/model/share_right.dart';
import 'package:psono/services/crypto_library.dart' as cryptoLibrary;

class AddFolderScreen extends StatefulWidget {
  static String tag = 'add-folder-screen';
  final datastoreModel.Folder parent;
  final datastoreModel.Datastore datastore;
  final datastoreModel.Folder share;
  final List<String> path;
  final List<String> relativePath;

  AddFolderScreen(
      {this.parent, this.datastore, this.share, this.path, this.relativePath});

  @override
  _AddFolderScreenState createState() => _AddFolderScreenState();
}

class _AddFolderScreenState extends State<AddFolderScreen> {
  final folderName = TextEditingController(
    text: '',
  );

  String message;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    folderName?.dispose();
    super.dispose();
  }

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    Widget messageWidget = Container();
    if (message != null) {
      messageWidget = component.AlertInfo(
        text: message,
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(FlutterI18n.translate(
          context,
          'NEW_FOLDER',
        )),
        elevation: 0,
        backgroundColor: Colors.white,
      ),
      backgroundColor: Color(0xFFebeeef),
      body: new Container(
        padding: new EdgeInsets.all(20.0),
        child: Form(
          key: _formKey,
          child: ListView(
            children: <Widget>[
              new TextFormField(
                controller: folderName,
                validator: (value) {
                  if (value.isEmpty) {
                    return FlutterI18n.translate(
                      context,
                      'FOLDER_NAME_IS_REQUIRED',
                    );
                  }
                  return null;
                },
                decoration: new InputDecoration(
                  labelText: FlutterI18n.translate(
                    context,
                    'FOLDER_NAME',
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: component.BtnSuccess(
                  onPressed: () async {
                    if (!_formKey.currentState.validate()) {
                      return;
                    }

                    if (this.widget.parent.folders == null) {
                      this.widget.parent.folders = [];
                    }

                    datastoreModel.Folder newFolder = new datastoreModel.Folder(
                      id: await cryptoLibrary.generateUUID(),
                      name: folderName.text,
                    );

                    if (this.widget.share == null) {
                      newFolder.shareRights = new ShareRight(
                        read: true,
                        write: true,
                        grant: true,
                        delete: true,
                      );
                    } else {
                      newFolder.shareRights = new ShareRight(
                        read: this.widget.share.shareRights.read,
                        write: this.widget.share.shareRights.write,
                        grant: this.widget.share.shareRights.grant &&
                            this.widget.share.shareRights.write,
                        delete: this.widget.share.shareRights.write,
                      );
                    }

                    this.widget.parent.folders.add(newFolder);

                    if (this.widget.share == null) {
                      datastoreModel.Datastore datastore =
                          await managerDatastorePassword.getPasswordDatastore(
                        this.widget.datastore.datastoreId,
                      );

                      datastoreModel.Folder parent;
                      if (this.widget.path.length == 0) {
                        parent = datastore.data;
                      } else {
                        List<String> pathCopy =
                            new List<String>.from(this.widget.path);
                        List search = managerDatastorePassword.findInDatastore(
                          pathCopy,
                          datastore.data,
                        );
                        parent = search[0][search[1]];
                      }

                      if (parent.folders == null) {
                        parent.folders = [];
                      }
                      parent.folders.add(newFolder);
                      await datastore.save();
                    } else {
                      shareModel.Share share = await managerShare.readShare(
                        this.widget.share.shareId,
                        this.widget.share.shareSecretKey,
                      );
                      datastoreModel.Folder parent;
                      if (this.widget.relativePath.length == 0) {
                        parent = share.folder;
                      } else {
                        List<String> pathCopy = new List<String>.from(
                          this.widget.relativePath,
                        );
                        List search = managerDatastorePassword.findInDatastore(
                          pathCopy,
                          share.folder,
                        );
                        parent = search[0][search[1]];
                      }
                      if (parent.folders == null) {
                        parent.folders = [];
                      }
                      parent.folders.add(newFolder);
                      await share.save();
                    }

                    setState(() {
                      message = FlutterI18n.translate(context, "SAVE_SUCCESS");
                    });
                    Navigator.pop(context);
                  },
                  text: FlutterI18n.translate(context, "SAVE"),
                ),
              ),
              messageWidget,
            ],
          ),
        ),
      ),
    );
  }
}
