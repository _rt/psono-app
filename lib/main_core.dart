import 'package:flutter/material.dart';

import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_i18n/flutter_i18n_delegate.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:redux/redux.dart';

import 'package:psono/theme.dart';
import 'package:psono/redux/store.dart';
import 'package:psono/screens/account/index.dart';
import 'package:psono/screens/autofill_onboarding/index.dart';
import 'package:psono/screens/datastore/index.dart';
import 'package:psono/screens/home/index.dart';
import 'package:psono/screens/lost_password/index.dart';
import 'package:psono/screens/passphrase/index.dart';
import 'package:psono/screens/privacy_policy/index.dart';
import 'package:psono/screens/register/index.dart';
import 'package:psono/screens/settings/index.dart';
import 'package:psono/screens/signin/index.dart';
import 'package:psono/app_config.dart';
import 'package:psono/protected_screen.dart';
import 'package:psono/model/app_state.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var config = AppConfig.of(context);
    final Store store = reduxStore;

    return StoreProvider<AppState>(
      store: store,
      child: new MaterialApp(
        title: config.appName,
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          canvasColor: primarySwatch.shade500,
          brightness: Brightness.light,
          accentColor: primarySwatch.shade500,
          primarySwatch: primarySwatch,
          fontFamily: 'Open Sans',
        ),
        initialRoute: '/',
        routes: {
          '/': (context) => HomeScreen(),
          '/account/': (context) => ProtectedScreen(child: AccountScreen()),
          '/autofill/': (context) => ProtectedScreen(child: DatastoreScreen()),
          '/datastore/': (context) => ProtectedScreen(child: DatastoreScreen()),
          '/autofill_onboarding/': (context) => AutofillOnboardingScreen(),
          '/lost_password/': (context) => LostPasswordScreen(),
          '/passphrase/': (context) => PassphraseScreen(),
          '/privacy_policy/': (context) => PrivacyPolicyScreen(),
          '/settings/': (context) => ProtectedScreen(child: SettingsScreen()),
          '/signin/': (context) => SigninScreen(),
          '/register/': (context) => RegisterScreen(),
        },
        localizationsDelegates: [
          FlutterI18nDelegate(
              useCountryCode: true,
              fallbackFile: 'en_US',
              path: "assets/locales"),
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate
        ],
      ),
    );
  }
}
