import 'package:flutter/material.dart';
import 'package:psono/redux/store.dart';
import 'package:psono/screens/passphrase/index.dart';

class ProtectedScreen extends StatefulWidget {
  final Widget child;
  ProtectedScreen({Key key, this.child}) : super(key: key);
  _ProtectedScreenState createState() => _ProtectedScreenState();
}

class _ProtectedScreenState extends State<ProtectedScreen>
    with WidgetsBindingObserver {
  int pauseEnabledTime;

  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    super.initState();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    int lockscreenThreshhold =
        3 * 60 * 1000; // will lock the screen after 3 minutes
    if (reduxStore.state.lockscreenEnabled &&
        state == AppLifecycleState.resumed &&
        pauseEnabledTime != null &&
        (DateTime.now().millisecondsSinceEpoch - pauseEnabledTime) >
            lockscreenThreshhold) {
      Navigator.of(context).push(
        MaterialPageRoute(builder: (context) => PassphraseScreen()),
      );
    }
    if (state == AppLifecycleState.paused) {
      pauseEnabledTime = DateTime.now().millisecondsSinceEpoch;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: widget.child,
    );
  }
}
