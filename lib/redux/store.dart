import 'package:redux/redux.dart';

import 'package:psono/model/app_state.dart';
import 'package:psono/redux/reducers.dart';

final Store reduxStore = Store<AppState>(
  appReducer,
  initialState: AppState.initialState(),
  middleware: [],
);
