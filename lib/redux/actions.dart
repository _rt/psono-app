import 'dart:typed_data';
import 'package:psono/model/config.dart';

class InitiateStateAction {
  final String serverUrl;
  final String username;
  final String token;
  final Uint8List sessionSecretKey;
  final Uint8List secretKey;
  final Uint8List publicKey;
  final Uint8List privateKey;
  final String lockscreenPassphrase;
  final bool lockscreenEnabled;
  final bool complianceDisableDeleteAccount;
  final String userId;
  final String userEmail;
  final String userSauce;
  final Config config;
  InitiateStateAction(
    this.serverUrl,
    this.username,
    this.token,
    this.sessionSecretKey,
    this.secretKey,
    this.publicKey,
    this.privateKey,
    this.lockscreenPassphrase,
    this.lockscreenEnabled,
    this.complianceDisableDeleteAccount,
    this.userId,
    this.userEmail,
    this.userSauce,
    this.config,
  );
}

class SetUserEmailAction {
  final String userEmail;
  SetUserEmailAction(
    this.userEmail,
  );
}

class InitiateLoginAction {
  final String serverUrl;
  final String username;
  InitiateLoginAction(
    this.serverUrl,
    this.username,
  );
}

class ConfigUpdatedAction {
  final Config config;
  ConfigUpdatedAction(
    this.config,
  );
}

class SetVerifyKeyAction {
  final Uint8List verifyKeyOld;
  final Uint8List verifyKey;
  final bool complianceDisableDeleteAccount;
  SetVerifyKeyAction(
    this.verifyKeyOld,
    this.verifyKey,
    this.complianceDisableDeleteAccount,
  );
}

class SetSessionInfoAction {
  final String userId;
  final String userEmail;
  final String token;
  final Uint8List sessionSecretKey;
  final Uint8List secretKey;
  final Uint8List publicKey;
  final Uint8List privateKey;
  final String userSauce;
  SetSessionInfoAction(
    this.userId,
    this.userEmail,
    this.token,
    this.sessionSecretKey,
    this.secretKey,
    this.publicKey,
    this.privateKey,
    this.userSauce,
  );
}

class UpdateLockscreenSettingAction {
  final String lockscreenPassphrase;
  final bool lockscreenEnabled;
  UpdateLockscreenSettingAction(
    this.lockscreenPassphrase,
    this.lockscreenEnabled,
  );
}

class PasswordGeneratorSettingAction {
  final String passwordLength;
  final String lettersUppercase;
  final String lettersLowercase;
  final String numbers;
  final String specialChars;
  PasswordGeneratorSettingAction(
    this.passwordLength,
    this.lettersUppercase,
    this.lettersLowercase,
    this.numbers,
    this.specialChars,
  );
}
