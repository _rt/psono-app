class BadRequestException implements Exception {
  final int statusCode = 400;
  final String message;

  BadRequestException(this.message);

  String toString() => message;
}

class UnauthorizedException implements Exception {
  final int statusCode = 401;
  final String message;

  UnauthorizedException(this.message);

  String toString() => message;
}

class ForbiddenException implements Exception {
  final int statusCode = 403;
  final String message;

  ForbiddenException(this.message);

  String toString() => message;
}

class InternalServerErrorException implements Exception {
  final int statusCode = 500;
  final String message;

  InternalServerErrorException(this.message);

  String toString() => message;
}

class ServiceUnavailableException implements Exception {
  final int statusCode = 503;
  final String message;

  ServiceUnavailableException(this.message);

  String toString() => message;
}

class UnknownException implements Exception {
  final int statusCode;
  final String message;

  UnknownException(this.message, this.statusCode);

  String toString() => message;
}
