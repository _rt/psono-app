import 'package:json_annotation/json_annotation.dart';

part 'yubikey_otp_verify.g.dart';

@JsonSerializable()
class YubikeyOtpVerify {
  YubikeyOtpVerify();

  factory YubikeyOtpVerify.fromJson(Map<String, dynamic> json) =>
      _$YubikeyOtpVerifyFromJson(json);

  Map<String, dynamic> toJson() => _$YubikeyOtpVerifyToJson(this);
}
