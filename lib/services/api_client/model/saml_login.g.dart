// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'saml_login.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SamlLogin _$SamlLoginFromJson(Map<String, dynamic> json) {
  return SamlLogin(
    loginInfo: fromHex(json['login_info'] as String),
    loginInfoNonce: fromHex(json['login_info_nonce'] as String),
  );
}

Map<String, dynamic> _$SamlLoginToJson(SamlLogin instance) => <String, dynamic>{
      'login_info': toHex(instance.loginInfo),
      'login_info_nonce': toHex(instance.loginInfoNonce),
    };
