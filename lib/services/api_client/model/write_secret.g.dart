// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'write_secret.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WriteSecret _$WriteSecretFromJson(Map<String, dynamic> json) {
  return WriteSecret(
    secretId: json['secret_id'] as String,
  );
}

Map<String, dynamic> _$WriteSecretToJson(WriteSecret instance) =>
    <String, dynamic>{
      'secret_id': instance.secretId,
    };
