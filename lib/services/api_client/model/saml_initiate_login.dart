import 'package:json_annotation/json_annotation.dart';

part 'saml_initiate_login.g.dart';

@JsonSerializable()
class SamlInitiateLogin {
  SamlInitiateLogin({
    this.samlRedirectUrl,
  });

  @JsonKey(name: 'saml_redirect_url')
  final String samlRedirectUrl;

  factory SamlInitiateLogin.fromJson(Map<String, dynamic> json) =>
      _$SamlInitiateLoginFromJson(json);

  Map<String, dynamic> toJson() => _$SamlInitiateLoginToJson(this);
}
