// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'oidc_login.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OidcLogin _$OidcLoginFromJson(Map<String, dynamic> json) {
  return OidcLogin(
    loginInfo: fromHex(json['login_info'] as String),
    loginInfoNonce: fromHex(json['login_info_nonce'] as String),
  );
}

Map<String, dynamic> _$OidcLoginToJson(OidcLogin instance) => <String, dynamic>{
      'login_info': toHex(instance.loginInfo),
      'login_info_nonce': toHex(instance.loginInfoNonce),
    };
