// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'create_secret.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CreateSecret _$CreateSecretFromJson(Map<String, dynamic> json) {
  return CreateSecret(
    secretId: json['secret_id'] as String,
  );
}

Map<String, dynamic> _$CreateSecretToJson(CreateSecret instance) =>
    <String, dynamic>{
      'secret_id': instance.secretId,
    };
