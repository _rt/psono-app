import 'package:psono/services/api_client/index.dart' as apiClient;
import 'package:psono/redux/store.dart';

/// Delete a file link
///
/// Returns a promise with the status of the delete operation
Future<apiClient.DeleteFileLink> deleteFileLink(String linkId) async {
  return await apiClient.deleteFileLink(
    reduxStore.state.token,
    reduxStore.state.sessionSecretKey,
    linkId,
  );
}

/// triggered once a file is deleted.
///
/// Returns noting
Future<void> onFileDeleted(String linkId) async {
  await deleteFileLink(linkId);
}
