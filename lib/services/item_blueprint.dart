import 'package:psono/model/blueprint.dart';
import 'package:psono/components/_index.dart' as component;

final Blueprint _blueprintWebsitePassword = Blueprint(
  id: "website_password", // Unique ID
  name: "WEBSITE_PASSWORD", // Displayed in Dropdown Menu
  icon: component.FontAwesome.key,
  titleField:
      "website_password_title", // is the main column, that is used as filename
  urlfilterField:
      "website_password_url_filter", // is the filter column for url matching
  autosubmitField:
      "website_password_auto_submit", // is the filter column for auto submit
  search: [
    'website_password_title',
    'website_password_url_filter',
  ], // are searched when the user search his entries
  fields: [
    BlueprintField(
      name: "website_password_title",
      field: "input",
      type: "text",
      title: "TITLE",
      placeholder: "TITLE",
      required: true,
      errorMessageRequired: 'TITLE_IS_REQUIRED',
    ),
    BlueprintField(
      name: "website_password_url",
      field: "input",
      type: "text",
      validationType: "url",
      title: "URL",
      placeholder: "URL",
      onChange: "onChangeUrl",
    ),
    BlueprintField(
      name: "website_password_username",
      field: "input",
      type: "text",
      title: "USERNAME",
      placeholder: "USERNAME",
    ),
    BlueprintField(
        name: "website_password_password",
        field: "input",
        type: "password",
        title: "PASSWORD",
        placeholder: "PASSWORD",
        dropDownMenuItems: [
          DropDownMenuItem(
            icon: component.FontAwesome.eye_slash,
            text: "SHOW_PASSWORD",
            onclick: () {
              print('TODO Blueprint SHOW_PASSWORD');
            },
          ),
          DropDownMenuItem(
            icon: component.FontAwesome.cogs,
            text: "GENERATE_PASSWORD",
            hideOffline: true,
            hideOnNotWrite: true,
            onclick: () {
              print('TODO Blueprint GENERATE_PASSWORD');
            },
          ),
        ]),
    BlueprintField(
      name: "website_password_notes",
      field: "textarea",
      title: "NOTES",
      placeholder: "NOTES",
    ),
    BlueprintField(
      name: "website_password_auto_submit",
      field: "input",
      type: "checkbox",
      title: "AUTOMATIC_SUBMIT",
      position: "advanced",
    ),
    BlueprintField(
      name: "website_password_url_filter",
      field: "textarea",
      title: "DOMAIN_FILTER",
      placeholder: "URL_FILTER_EG",
      position: "advanced",
    ),
  ],
);

final Blueprint _blueprintApplicationPassword = Blueprint(
  id: "application_password", // Unique ID
  name: "APPLICATION_PASSWORD", // Displayed in Dropdown Menu
  icon: component.FontAwesome.key,
  titleField:
      "application_password_title", // is the main column, that is used as filename
  search: [
    'application_password_title',
  ], // are searched when the user search his entries
  fields: [
    BlueprintField(
      name: "application_password_title",
      field: "input",
      type: "text",
      title: "TITLE",
      placeholder: "TITLE",
      required: true,
      errorMessageRequired: 'TITLE_IS_REQUIRED',
    ),
    BlueprintField(
      name: "application_password_username",
      field: "input",
      type: "text",
      title: "USERNAME",
      placeholder: "USERNAME",
    ),
    BlueprintField(
        name: "application_password_password",
        field: "input",
        type: "password",
        title: "PASSWORD",
        placeholder: "PASSWORD",
        dropDownMenuItems: [
          DropDownMenuItem(
            icon: component.FontAwesome.eye_slash,
            text: "SHOW_PASSWORD",
            onclick: () {
              print('TODO Blueprint SHOW_PASSWORD');
            },
          ),
          DropDownMenuItem(
            icon: component.FontAwesome.cogs,
            text: "GENERATE_PASSWORD",
            hideOffline: true,
            hideOnNotWrite: true,
            onclick: () {
              print('TODO Blueprint GENERATE_PASSWORD');
            },
          ),
        ]),
    BlueprintField(
      name: "application_password_notes",
      field: "textarea",
      title: "NOTES",
      placeholder: "NOTES",
    ),
  ],
);

final Blueprint _blueprintTOTP = Blueprint(
  id: "totp", // Unique ID
  name: "TOTP_AUTHENTICATOR", // Displayed in Dropdown Menu
  icon: component.FontAwesome.qrcode,
  titleField: "totp_title", // is the main column
  search: [
    'totp_title',
  ], // are searched when the user search his entries
  fields: [
    BlueprintField(
      name: "totp_title",
      field: "input",
      type: "text",
      title: "TITLE",
      placeholder: "TITLE",
      required: true,
      errorMessageRequired: 'TITLE_IS_REQUIRED',
    ),
    BlueprintField(
      name: "totp_code",
      field: "input",
      type: "totp_code",
      title: "SECRET",
      placeholder: "SECRET",
      dropDownMenuItems: [
        DropDownMenuItem(
          icon: component.FontAwesome.eye_slash,
          text: "SHOW",
          onclick: () {
            print('TODO Blueprint SHOW_PASSWORD');
          },
        ),
      ],
    ),
    BlueprintField(
      name: "totp_notes",
      field: "textarea",
      title: "NOTES",
      placeholder: "NOTES",
    ),
  ],
);

final Blueprint _blueprintNote = Blueprint(
  id: "note",
  name: "NOTE",
  icon: component.FontAwesome.sticky_note,
  titleField: "note_title",
  search: ['note_title'],
  fields: [
    BlueprintField(
      name: "note_title",
      field: "input",
      type: "text",
      title: "TITLE",
      placeholder: "TITLE",
      required: true,
      errorMessageRequired: 'TITLE_IS_REQUIRED',
    ),
    BlueprintField(
      name: "note_notes",
      field: "textarea",
      title: "NOTES",
      placeholder: "NOTES",
    ),
  ],
);

final Blueprint _blueprintBookmark = Blueprint(
  id: "bookmark", // Unique ID
  name: "BOOKMARK", // Displayed in Dropdown Menu
  icon: component.FontAwesome.bookmark_o,
  titleField: "bookmark_title", // is the main column, that is used as filename
  urlfilterField:
      "bookmark_url_filter", // is the filter column for url matching
  search: [
    'bookmark_title',
    'bookmark_url_filter'
  ], // are searched when the user search his entries
  fields: [
    // All fields for this object with unique names
    BlueprintField(
      name: "bookmark_title",
      field: "input",
      type: "text",
      title: "TITLE",
      placeholder: "TITLE",
      required: true,
      errorMessageRequired: 'TITLE_IS_REQUIRED',
    ),
    BlueprintField(
      name: "bookmark_url",
      field: "input",
      type: "text",
      validationType: "url",
      title: "URL",
      placeholder: "URL",
      onChange: "onChangeUrl",
    ),
    BlueprintField(
      name: "bookmark_notes",
      field: "textarea",
      title: "NOTES",
      placeholder: "NOTES",
    ),
    BlueprintField(
      name: "bookmark_url_filter",
      field: "textarea",
      title: "DOMAIN_FILTER",
      placeholder: "URL_FILTER_EG",
      position: "advanced",
    ),
  ],
);

final Blueprint _blueprintMailGPGOwnKey = Blueprint(
  id: "mail_gpg_own_key",
  name: "GPG_KEY",
  icon: component.FontAwesome.lock,
  titleField: "mail_gpg_own_key_title",
  search: ['mail_gpg_own_key_title', 'mail_gpg_own_key_email'],
  fields: [
    BlueprintField(
      name: "mail_gpg_own_key_title",
      field: "input",
      type: "text",
      title: "TITLE",
      hidden: true,
      placeholder: "TITLE",
      required: true,
      errorMessageRequired: 'TITLE_IS_REQUIRED',
    ),
    BlueprintField(
      name: "mail_gpg_own_key_email",
      field: "input",
      type: "text",
      title: "EMAIL",
      placeholder: "EMAIL",
      hidden: true,
      readonly: true,
    ),
    BlueprintField(
      name: "mail_gpg_own_key_name",
      field: "input",
      type: "text",
      title: "NAME",
      placeholder: "NAME",
      hidden: true,
      readonly: true,
    ),
    BlueprintField(
      name: "mail_gpg_own_key_public",
      field: "textarea",
      title: "PUBLIC_KEY",
      placeholder: "PUBLIC_KEY",
      hidden: true,
      readonly: true,
    ),
    BlueprintField(
      name: "mail_gpg_own_key_private",
      field: "textarea",
      title: "PRIVATE_KEY",
      placeholder: "PRIVATE_KEY",
      hidden: true,
      readonly: true,
    ),
    BlueprintField(
      name: "mail_gpg_own_key_publish",
      field: "input",
      type: "checkbox",
      title: "PUBLISH_PUBLIC_KEY",
      hidden: true,
    ),
    BlueprintField(
      name: "mail_gpg_own_key_generate_new",
      field: "button",
      type: "button",
      title: "GENERATE_NEW_GPG_KEY",
      hidden: true,
      classname: 'btn-primary',
      onClick: "onClickGenerateNewButton",
    ),
    BlueprintField(
      name: "mail_gpg_own_key_generate_import_text",
      field: "button",
      type: "button",
      title: "IMPORT_AS_TEXT",
      hidden: true,
      classname: 'btn-primary',
      onClick: "onClickImportAsTextButton",
    ),
    BlueprintField(
      name: "mail_gpg_own_key_encrypt_message",
      field: "button",
      type: "button",
      title: "ENCRYPT_MESSAGE",
      hidden: true,
      classname: 'btn-default',
      onClick: "onClickEncryptMessageButton",
    ),
    BlueprintField(
      name: "mail_gpg_own_key_decrypt_message",
      field: "button",
      type: "button",
      title: "DECRYPT_MESSAGE",
      hidden: true,
      classname: 'btn-default',
      onClick: "onClickDecryptMessageButton",
    ),
  ],
);

Map<String, Blueprint> _blueprints = {
  'application_password': _blueprintApplicationPassword,
  'website_password': _blueprintWebsitePassword,
  'totp': _blueprintTOTP,
  'note': _blueprintNote,
  'mail_gpg_own_key': _blueprintMailGPGOwnKey,
  'bookmark': _blueprintBookmark,
};

/// Returns the blueprint with the given [key] otherwise null
Blueprint getBlueprint(String key) {
  if (_blueprints.containsKey(key)) {
    return _blueprints[key];
  }
  return null;
}

/// returns an overview of all available blueprints with name and id
List<Blueprint> getBlueprints() {
  List<Blueprint> result = [];

  _blueprints.forEach((String type, Blueprint blueprint) {
    result.add(blueprint);
  });

  return result;
}
