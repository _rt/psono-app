import 'package:device_info/device_info.dart';
import 'package:psono/services/crypto_library.dart' as cryptoLibrary;
import 'package:psono/services/storage.dart';
import 'dart:io' show Platform;

String deviceFingerprint;

/// Returns the device fingerprint
///
/// @returns Returns the fingerprint
Future<String> getDeviceFingerprint() async {
  if (deviceFingerprint != null) {
    return deviceFingerprint;
  }

  deviceFingerprint = await storage.read(key: 'deviceFingerprint');

  if (deviceFingerprint == null) {
    deviceFingerprint = await cryptoLibrary.generateUUID();
    storage.write(
      key: 'deviceFingerprint',
      value: deviceFingerprint,
      iOptions: secureIOSOptions,
    );
  }

  return deviceFingerprint;
}

/// Returns the device description
///
/// @returns Returns the description
Future<String> getDeviceDescription() async {
  DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
  String deviceDescription = '';

  if (Platform.isAndroid) {
    final AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
    deviceDescription = androidInfo.model;
  } else if (Platform.isIOS) {
    final IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
    deviceDescription = iosInfo.localizedModel;
  }

  return deviceDescription;
}
