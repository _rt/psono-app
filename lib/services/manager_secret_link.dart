import 'package:psono/services/api_client/index.dart' as apiClient;
import 'package:psono/redux/store.dart';

/// Delete a secret link
///
/// Returns a promise with the status of the delete operation
Future<apiClient.DeleteSecretLink> deleteSecretLink(String linkId) async {
  return await apiClient.deleteSecretLink(
    reduxStore.state.token,
    reduxStore.state.sessionSecretKey,
    linkId,
  );
}

/// triggered once a secret is deleted.
///
/// Returns noting
Future<void> onSecretDeleted(String linkId) async {
  await deleteSecretLink(linkId);
}
