import 'dart:typed_data';

import 'package:psono/model/share.dart' as shareModel;
import 'package:psono/model/share_right.dart' as shareRightModel;
import 'package:psono/model/datastore.dart' as datastoreModel;
import 'package:psono/services/api_client/index.dart' as apiClient;
import 'package:psono/services/manager_datastore.dart' as managerDatastore;
import 'package:psono/services/manager_datastore_setting.dart'
    as managerDatastoreSetting;
import 'package:psono/services/manager_share.dart' as managerShare;
import 'package:psono/services/settings.dart' as settings;
import 'package:psono/services/crypto_library.dart' as cryptoLibrary;
import 'package:psono/services/helper.dart' as helper;

Map<String, Uint8List> _shareIndex = {};
int uppercaseMinCount = 1;
int lowercaseMinCount = 1;
int numberMinCount = 1;
int specialMinCount = 1;

class ObjectNotFound implements Exception {
  final String message;

  ObjectNotFound([this.message]);

  String toString() => message;
}

List findObject(datastoreModel.Folder folder, String searchId) {
  var n, l;

  if (folder.folders != null) {
    // check if the object is a folder, if yes return the folder list and the index
    n = 0;
    for (l = folder.folders.length; n < l; n++) {
      if (folder.folders[n].id == searchId) {
        return [folder.folders, n];
      }
    }
  }
  if (folder.items != null) {
    // check if its a file, if yes return the file list and the index
    n = 0;
    for (l = folder.items.length; n < l; n++) {
      if (folder.items[n].id == searchId) {
        return [folder.items, n];
      }
    }
  }

  // something went wrong, couldn't find the item / folder here
  throw ObjectNotFound();
}

class ChildItem {
  final String id;
  final List<String> path;
  const ChildItem(this.id, this.path);
}

class ChildShare {
  final String shareId;
  final datastoreModel.Folder folder;
  final datastoreModel.Item item;
  final List<String> path;
  const ChildShare(this.shareId, this.folder, this.item, this.path);
}

typedef ComparisonFunction<T> = bool Function(T obj);

/// triggered once a share is deleted. Searches the datastore for the closest
/// share (or the datastore if no share) and removes it from the share_index
///
/// Returns noting
void onShareDeleted(ChildShare childShare,
    datastoreModel.Datastore parentDatastore, shareModel.Share parentShare) {
  List<String> childSharePath = []..addAll(childShare.path);
  String shareId = childShare.shareId;

  datastoreModel.Folder folder;
  if (parentDatastore != null &&
      parentDatastore.data.shareIndex.containsKey(shareId)) {
    folder = parentDatastore.data;
  }
  if (parentShare != null &&
      parentShare.folder.shareIndex.containsKey(shareId)) {
    folder = parentShare.folder;
  }

  datastoreModel.ShareLocation shareLocations = folder.shareIndex[shareId];

  var alreadyFound = false;

  for (var i = shareLocations.paths.length - 1; i >= 0; i--) {
    // delete the path from the share index entry
    if (helper.arrayStartsWith(shareLocations.paths[i], childSharePath)) {
      shareLocations.paths.removeAt(i);
      alreadyFound = true;
    }
    // if no paths are empty, we delete the whole share_index entry
    if (shareLocations.paths.length == 0) {
      folder.shareIndex.remove(shareId);
    }
    // if the share_index holds no entries anymore, we delete the share_index
    if (folder.shareIndex.length == 0) {
      folder.shareIndex = null;
    }

    if (alreadyFound) {
      break;
    }
  }
}

/// fills other_children with all child shares of a given path
void getAllChildShares(
  var obj,
  int shareDistance,
  List<ChildShare> otherChildren,
  List<String> path,
) {
  if (path == null) {
    path = [];
  }

  if (shareDistance == 0) {
    return;
  }

  //search in folders
  if (!(obj is datastoreModel.Folder)) {
    return;
  }
  if (obj.folders != null) {
    for (int n = 0; n < obj.folders.length; n++) {
      List<String> newPath = List.from(path);
      newPath.add(obj.folders[n].id);
      if (obj.folders[n].shareId != null) {
        otherChildren.add(new ChildShare(
          obj.folders[n].shareId,
          obj.folders[n],
          null,
          newPath,
        ));
        getAllChildShares(
          obj.folders[n],
          shareDistance - 1,
          otherChildren,
          newPath,
        );
      } else {
        getAllChildShares(
          obj.folders[n],
          shareDistance,
          otherChildren,
          newPath,
        );
      }
    }
  }
  if (obj.items != null) {
    for (int n = 0; n < obj.items.length; n++) {
      List<String> newPath = List.from(path);
      newPath.add(obj.items[n].id);
      if (obj.items[n].shareId != null) {
        otherChildren.add(
            new ChildShare(obj.items[n].shareId, null, obj.items[n], newPath));
      }
    }
  }
}

List<ChildItem> getAllElementsWhichMatch(
    var element, ComparisonFunction compare) {
  List<ChildItem> links = [];

  getAllElementsWhichMatchRecursive(
      var element, List<ChildItem> links, List<String> path) {
    List<String> newPath = List.from(path);
    newPath.add(element.id);

    if (element.shareId != null) {
      return;
    }

    // check if the element itself has the property
    if (compare(element)) {
      links.add(new ChildItem(element.id, newPath));
    }

    if (!(element is datastoreModel.Folder)) {
      return;
    }

    // search items recursive, skip shares
    if (element.items != null) {
      for (int n = 0, l = element.items.length; n < l; n++) {
        if (element.items[n].shareId != null) {
          continue;
        }
        getAllElementsWhichMatchRecursive(element.items[n], links, newPath);
      }
    }

    // search folders recursive, skip shares
    if (element.folders != null) {
      for (int n = 0, l = element.folders.length; n < l; n++) {
        if (element.folders[n].shareId != null) {
          continue;
        }
        getAllElementsWhichMatchRecursive(element.folders[n], links, newPath);
      }
    }
  }

  getAllElementsWhichMatchRecursive(element, links, []);

  return links;
}

/// returns all secret links in element. Doesn't cross share borders.
List<ChildItem> getAllSecretLinks(element) {
  compare(var obj) {
    if (obj is datastoreModel.Item) {
      return obj.secretId != null;
    }
    return false;
  }

  return getAllElementsWhichMatch(element, compare);
}

/// returns all file links in element. Doesn't cross share borders
List<ChildItem> getAllFileLinks(element) {
  compare(var obj) {
    if (obj is datastoreModel.Item) {
      return obj.fileId != null;
    }
    return false;
  }

  return getAllElementsWhichMatch(element, compare);
}

/// fills other_children with all child shares of a given path
void getAllChildSharesByPath(
  List<String> path,
  datastoreModel.Folder datastore,
  List<ChildShare> otherChildren,
  var obj,
) {
  if (obj == null) {
    List<String> pathCopy = List.from(path);
    var search = findInDatastore(pathCopy, datastore);
    obj = search[0][search[1]];
    return getAllChildShares(obj, 1, otherChildren, path);
  } else {
    getAllChildShares(obj, 1, otherChildren, path);
  }
}

/// Go through the datastore recursive to find the object specified with the path
List findInDatastore(
  List<String> path,
  datastoreModel.Folder datastore,
) {
  List<String> rest = new List<String>.from(path);
  final String toSearch = rest.removeAt(0);
  int n, l;

  if (rest.length == 0) {
    return findObject(datastore, toSearch);
  }

  n = 0;
  l = datastore.folders.length;

  for (; n < l; n++) {
    if (datastore.folders[n].id == toSearch) {
      return findInDatastore(rest, datastore.folders[n]);
    }
  }
  // something went wrong, couldn't find the item / folder here
  throw ObjectNotFound();
}

/// Sets the parent for folders and items, based on the obj and obj parents.
/// Calls recursive itself for all folders and skips nested shares
void updateParents(
  datastoreModel.Item item,
  datastoreModel.Folder folder,
  parentShareId,
  parentDatastoreId,
) {
//  var n;
//
//  var new_parent_share_id = parent_share_id;
//  var new_parent_datastore_id = parent_datastore_id;
//
//  if (obj.hasOwnProperty('datastore_id')) {
//    obj['parent_share_id'] = undefined;
//    obj['parent_datastore_id'] = undefined;
//    new_parent_share_id = undefined;
//    new_parent_datastore_id = obj.datastore_id;
//  } else if (obj.hasOwnProperty('share_id')) {
//    obj['parent_share_id'] = parent_share_id;
//    obj['parent_datastore_id'] = parent_datastore_id;
//    new_parent_share_id = obj.share_id;
//    new_parent_datastore_id = undefined;
//  }
//
//  // check all folders recursive
//  if (obj.hasOwnProperty('folders')) {
//    for (n = 0; n < obj.folders.length; n++) {
//      obj.folders[n]['parent_share_id'] = new_parent_share_id;
//      obj.folders[n]['parent_datastore_id'] = new_parent_datastore_id;
//
//      // lets not go inside of a new share, and dont touch the parents there
//      if (obj.folders[n].hasOwnProperty('share_id')) {
//        continue;
//      }
//      update_parents(obj.folders[n], new_parent_share_id, new_parent_datastore_id);
//    }
//  }
//  // check all items
//  if (obj.hasOwnProperty('items')) {
//    for (n = 0; n < obj.items.length; n++) {
//      if (obj.items[n].hasOwnProperty('share_id')) {
//        continue;
//      }
//      obj.items[n]['parent_share_id'] = new_parent_share_id;
//      obj.items[n]['parent_datastore_id'] = new_parent_datastore_id;
//    }
//  }
}

/// Updates some datastore folders or share folders with content.
/// Will calculate the delete property in the right object.
void updatePathsWithData(
  datastoreModel.Folder datastore,
  List<String> path,
  shareModel.Share content,
  shareRightModel.ShareRight parentShareRights,
  String parentShareId,
  String parentDatastoreId,
) {
  List<String> pathCopy = new List<String>.from(path);
  List search = findInDatastore(pathCopy, datastore);

  if (search[0][search[1]] is datastoreModel.Folder) {
    datastoreModel.Folder newFolderShare = search[0][search[1]];

    // update share_rights in share object
    newFolderShare.shareRights = shareRightModel.ShareRight(
      read: content.rights.read,
      write: content.rights.write,
      grant: content.rights.grant,
      delete: parentShareRights.write,
    );

    newFolderShare.name = content.folder.name;
    newFolderShare.folders = content.folder.folders;
    newFolderShare.items = content.folder.items;
    newFolderShare.shareIndex = content.folder.shareIndex;

    // update share_rights in folders and items
    updateParents(null, newFolderShare, parentShareId, parentDatastoreId);
    managerDatastore.updateShareRightsOfFoldersAndItems(
      newFolderShare,
      shareRightModel.ShareRight(
        read: true,
        write: true,
        grant: true,
        delete: true,
      ),
    );
  } else {
    datastoreModel.Item newItemShare = search[0][search[1]];

    // update share_rights in share object
    newItemShare.shareRights = shareRightModel.ShareRight(
      read: content.rights.read,
      write: content.rights.write,
      grant: content.rights.grant,
      delete: parentShareRights.write,
    );

    newItemShare.name = content.item.name;
    newItemShare.type = content.item.type;
    newItemShare.urlfilter = content.item.urlfilter;
    newItemShare.secretId = content.item.secretId;
    newItemShare.secretKey = content.item.secretKey;

    // update share_rights in folders and items
    updateParents(newItemShare, null, parentShareId, parentDatastoreId);
  }
}

/// Queries shares recursive. Returns promise that resolves either when the
/// initial datastore is loaded or when all shares with subshares are loaded
Future<datastoreModel.Datastore> _readShares(
  datastoreModel.Datastore datastore,
  Map<String, apiClient.ReadShareRightsListEntry> shareRightsDict,
) async {
  if (datastore.data == null || datastore.data.shareIndex == null) {
    return datastore;
  }

  var shareIndex = datastore.data.shareIndex;
  shareModel.Share content;

  Map<String, shareModel.Share> allShareData = {};

  shareRightModel.ShareRight parentShareRights = shareRightModel.ShareRight(
    read: true,
    write: true,
    grant: true,
    delete: true,
  );

  Future<void> readSharesRecursive(
    datastoreModel.Folder datastore,
    Map<String, apiClient.ReadShareRightsListEntry> shareRightsDict,
    Map<String, datastoreModel.ShareLocation> shareIndex,
    Map<String, shareModel.Share> allShareData,
    shareRightModel.ShareRight parentShareRights,
    String parentShareId,
    String parentDatastoreId,
    List<String> parentShareStack,
  ) async {
    List<Future> allCalls = [];
    if (shareIndex == null) {
      return;
    }

    shareIndex.forEach((String shareId, shareLocation) {
      _shareIndex[shareId] = shareLocation.secretKey;
      List<String> newParentShareStack =
          new List<String>.from(parentShareStack);
      newParentShareStack.add(shareId);

      Future readShareHelper(
          String shareId,
          datastoreModel.Folder folderShare,
          datastoreModel.Item itemShare,
          List<String> path,
          String parentShareId,
          String parentDatastoreId,
          parentShareStack) async {
        shareModel.Share content =
            await managerShare.readShare(shareId, _shareIndex[shareId]);

        allShareData[shareId] = content;

        updatePathsWithData(datastore, path, content, parentShareRights,
            parentShareId, parentDatastoreId);

        if (folderShare != null) {
          await readSharesRecursive(
              folderShare,
              shareRightsDict,
              content.folder.shareIndex,
              allShareData,
              content.rights,
              shareId,
              null,
              parentShareStack);
        }
      }

      for (var i = shareIndex[shareId].paths.length - 1; i >= 0; i--) {
        List<String> pathCopy =
            new List<String>.from(shareIndex[shareId].paths[i]);
        List search = findInDatastore(pathCopy, datastore);

        datastoreModel.Folder newFolderShare;
        datastoreModel.Item newItemShare;
        if (search[0][search[1]] is datastoreModel.Folder) {
          newFolderShare = search[0][search[1]];
        } else {
          newItemShare = search[0][search[1]];
        }

        // Break potential loops
        if (parentShareStack.contains(shareId)) {
          content = shareModel.Share(
            rights: shareRightModel.ShareRight(
              read: false,
              write: false,
              grant: false,
            ),
          );

          updatePathsWithData(datastore, shareIndex[shareId].paths[i], content,
              parentShareRights, parentShareId, null);
          continue;
        }
        if (allShareData.containsKey(shareId)) {
          updatePathsWithData(datastore, shareIndex[shareId].paths[i],
              allShareData[shareId], parentShareRights, parentShareId, null);
          continue;
        }

        // Let's check if we have read writes for this share, and skip it if we don't have read rights
        if (shareRightsDict.containsKey(shareId) &&
            !shareRightsDict[shareId].read) {
          content = shareModel.Share(
            rights: shareRightModel.ShareRight(
              read: shareRightsDict[shareId].read,
              write: shareRightsDict[shareId].write,
              grant: shareRightsDict[shareId].grant,
            ),
          );
          updatePathsWithData(datastore, shareIndex[shareId].paths[i], content,
              parentShareRights, parentShareId, null);
          continue;
        }

        // No specific share rights for this share, lets assume inherited rights and check if we have parent read rights
        if (!shareRightsDict.containsKey(shareId) && !parentShareRights.read) {
          content = shareModel.Share(
            rights: shareRightModel.ShareRight(
              read: parentShareRights.read,
              write: parentShareRights.write,
              grant: parentShareRights.grant,
            ),
          );

          updatePathsWithData(datastore, shareIndex[shareId].paths[i], content,
              parentShareRights, parentShareId, null);
          continue;
        }

        // No specific share rights for this share and datastore as parent (no inheritance possible) we a assume a share where we lost access rights
        if (!shareRightsDict.containsKey(shareId) &&
            parentDatastoreId != null) {
          continue;
        }

        allCalls.add(readShareHelper(
            shareId,
            newFolderShare,
            newItemShare,
            shareIndex[shareId].paths[i],
            parentShareId,
            parentDatastoreId,
            newParentShareStack));
      }
    });

    return await Future.wait(allCalls);
  }

  await readSharesRecursive(datastore.data, shareRightsDict, shareIndex,
      allShareData, parentShareRights, null, datastore.datastoreId, []);

  managerDatastore.updateShareRightsOfFoldersAndItems(
    datastore.data,
    new shareRightModel.ShareRight(
      read: true,
      write: true,
      grant: true,
      delete: true,
    ),
  );

  return datastore;
}

/// Sorts a folder recursive
Future sortFolderContent(datastoreModel.Folder folder) async {
  if (folder.folders != null) {
    folder.folders.sort((a, b) => a.name.compareTo(b.name));
    for (var i = 0; i < folder.folders.length; i++) {
      sortFolderContent(folder.folders[i]);
    }
  }

  if (folder.items != null) {
    folder.items.sort((a, b) {
      if (a.name == null) {
        return -1;
      }
      if (b.name == null) {
        return 1;
      }
      return a.name.compareTo(b.name);
    });
  }
}

/// Returns the password datastore. In addition this function triggers the
/// generation of the local datastore storage to. Returns a future with the
/// datastore and all shares filled recursive
Future<datastoreModel.Datastore> getPasswordDatastore([String id]) async {
  final String type = 'password';

  datastoreModel.Datastore datastore = await managerDatastore.getDatastore(
    type,
    id,
  );
  apiClient.ReadShareRightsList shareRightsOverview =
      await managerShare.readShareRightsOverview();
  Map<String, apiClient.ReadShareRightsListEntry> shareRightsDict = {};

  for (var i = 0; i < shareRightsOverview.shareRights.length; i++) {
    shareRightsDict[shareRightsOverview.shareRights[i].shareId] =
        shareRightsOverview.shareRights[i];
  }

  datastoreModel.Datastore filledDatastore = await _readShares(
    datastore,
    shareRightsDict,
  );

  managerDatastore.updateShareRightsOfFoldersAndItems(
    datastore.data,
    new shareRightModel.ShareRight(
      read: true,
      write: true,
      grant: true,
      delete: true,
    ),
  );

  if (filledDatastore.data != null) {
    sortFolderContent(filledDatastore.data);
  }

  return filledDatastore;
}

/// Searches all sub shares and hides (deletes) the content of those
/// Leaving only the properties in this list untouched:
/// ['id', 'name', 'shareId', 'shareSecretKey']
hideSubShareContent(datastoreModel.Folder share) {
  if (share.shareIndex == null) {
    return;
  }

  share.shareIndex.forEach((
    String shareId,
    datastoreModel.ShareLocation locations,
  ) {
    for (var i = locations.paths.length - 1; i >= 0; i--) {
      List<String> pathCopy = new List<String>.from(locations.paths[i]);

      List search = findInDatastore(pathCopy, share);

      if (search[0][search[1]] is datastoreModel.Folder) {
        datastoreModel.Folder newFolderShare = search[0][search[1]];
        newFolderShare.datastoreId = null;
        newFolderShare.folders = null;
        newFolderShare.items = null;
        newFolderShare.shareIndex = null;
        newFolderShare.shareRights = null;
      } else {
        datastoreModel.Item newItemShare = search[0][search[1]];
        newItemShare.type = null;
        newItemShare.urlfilter = null;
        newItemShare.secretId = null;
        newItemShare.secretKey = null;
        newItemShare.shareRights = null;
      }
    }
  });
}

/// escapes regex string
String escapeRegExp(String str) {
  return RegExp.escape(str);
}

/// checks if the given password complies with the minimal complexity
bool isStrongEnough(password) {
  if (uppercaseMinCount + lowercaseMinCount + numberMinCount + specialMinCount >
      int.parse(settings.getSetting('setting_password_length'))) {
    //password can never comply, so we skip check
    return true;
  }

  Iterable<RegExpMatch> uc = (new RegExp("([" +
          RegExp.escape(
              settings.getSetting('setting_password_letters_uppercase')) +
          "])"))
      .allMatches(password);
  Iterable<RegExpMatch> lc = (new RegExp("([" +
          RegExp.escape(
              settings.getSetting('setting_password_letters_lowercase')) +
          "])"))
      .allMatches(password);
  Iterable<RegExpMatch> n = (new RegExp("([" +
          RegExp.escape(settings.getSetting('setting_password_numbers')) +
          "])"))
      .allMatches(password);
  Iterable<RegExpMatch> sc = (new RegExp("([" +
          RegExp.escape(settings.getSetting('setting_password_special_chars')) +
          "])"))
      .allMatches(password);

  var ucTestResult =
      settings.getSetting('setting_password_letters_uppercase').length == 0 ||
          (uc.length >= uppercaseMinCount);
  var lcTestResult =
      settings.getSetting('setting_password_letters_lowercase').length == 0 ||
          (lc.length >= lowercaseMinCount);
  var nTestResult =
      settings.getSetting('setting_password_numbers').length == 0 ||
          (n.length >= numberMinCount);
  var scTestResult =
      settings.getSetting('setting_password_special_chars').length == 0 ||
          (sc.length >= specialMinCount);

  return ucTestResult && lcTestResult && nTestResult && scTestResult;
}

/// Main function to generate a random password based on the specified settings.
Future<String> generatePassword(int length, String allowedCharacters) async {
  var allowedCharactersLength = allowedCharacters.length;
  var password = '';

  for (var i = 0; i < length; i++) {
    var pos = (await cryptoLibrary.random() * allowedCharactersLength).floor();
    password = password + allowedCharacters[pos];
  }

  return password;
}

/// Main function to generate a random password based on the specified settings.
Future<String> generate() async {
  var password = "";
  await managerDatastoreSetting.getSettingsDatastore();
  while (!isStrongEnough(password)) {
    password = await generatePassword(
      int.parse(settings.getSetting('setting_password_length')),
      settings.getSetting('setting_password_letters_uppercase') +
          settings.getSetting('setting_password_letters_lowercase') +
          settings.getSetting('setting_password_numbers') +
          settings.getSetting('setting_password_special_chars'),
    );
  }
  return password;
}
