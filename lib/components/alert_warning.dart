import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

class AlertWarning extends StatelessWidget {
  final String text;

  AlertWarning({this.text});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 20.0),
      decoration: new BoxDecoration(
        color: Color(0xFFfcf8e3),
        border: Border.all(
          width: 1.0,
          color: Color(0xFFfaebcc),
        ),
        borderRadius: new BorderRadius.only(
          topLeft: const Radius.circular(5.0),
          topRight: const Radius.circular(5.0),
          bottomLeft: const Radius.circular(5.0),
          bottomRight: const Radius.circular(5.0),
        ),
      ),
      child: RichText(
        text: TextSpan(
          text: FlutterI18n.translate(context, "WARNING") + '! ',
          style: TextStyle(
            color: Color(0xFF8a6d3b),
            fontWeight: FontWeight.bold,
          ),
          children: <TextSpan>[
            TextSpan(
              text: text,
              style: TextStyle(
                fontWeight: FontWeight.normal,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
