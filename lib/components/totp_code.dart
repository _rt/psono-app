import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:otp/otp.dart';
import 'icons.dart';
import 'package:psono/services/helper.dart' as helper;

class TotpCode extends StatefulWidget {
  final String code;

  TotpCode({
    this.code,
  });

  @override
  _TotpCodeState createState() => _TotpCodeState();
}

class _TotpCodeState extends State<TotpCode> {
  int millisecondsSinceEpoch = DateTime.now().millisecondsSinceEpoch;
  int countdown =
      30 - ((DateTime.now().millisecondsSinceEpoch ~/ 1000).round() % 30);
  double percent = 1 -
      (30 - ((DateTime.now().millisecondsSinceEpoch ~/ 1000).round() % 30)) /
          30;
  Timer timer;
  String generatedCode = '';
  FToast _fToast;

  @override
  void initState() {
    super.initState();

    _fToast = FToast();
    _fToast.init(context);

    timer = Timer.periodic(Duration(milliseconds: 500), (timer) {
      setState(() {
        if (helper.isValidTOTPCode(this.widget.code)) {
          millisecondsSinceEpoch = DateTime.now().millisecondsSinceEpoch;
          countdown = 30 - ((millisecondsSinceEpoch ~/ 1000).round() % 30);
          percent = 1 - countdown / 30;
          generatedCode = OTP.generateTOTPCodeString(
            this.widget.code,
            millisecondsSinceEpoch,
            algorithm: Algorithm.SHA1,
            isGoogle: true,
          );
        } else {
          generatedCode = FlutterI18n.translate(context, 'INVALID_CODE');
        }
      });
    });
  }

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return CircularPercentIndicator(
      percent: percent,
      animation: true,
      lineWidth: 16.0,
      circularStrokeCap: CircularStrokeCap.round,
      animateFromLastPercent: true,
      radius: 240.0,
      progressColor: Color(0xFF5cb85c),
      center: new FlatButton(
        child: new RichText(
          text: TextSpan(
            children: [
              TextSpan(
                text: generatedCode,
              ),
              TextSpan(
                text: " ",
              ),
              WidgetSpan(
                child: Icon(
                  FontAwesome.clipboard,
                  size: 32,
                  color: Colors.black54,
                ),
              ),
            ],
            style: TextStyle(
              color: Colors.black54,
              fontSize: 32.0,
            ),
          ),
        ),
        onPressed: () {
          Clipboard.setData(
            new ClipboardData(
              text: generatedCode,
            ),
          );
          _clipboardCopyToast();
        },
      ),
    );
  }

  _clipboardCopyToast() {
    Widget toast = Container(
      padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(25.0),
        color: Color(0xFF5cb85c),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(
            FontAwesome.clipboard,
            color: Colors.white,
          ),
          SizedBox(
            width: 12.0,
          ),
          Text(
            FlutterI18n.translate(context, "CLIPBOARD_COPY"),
            style: TextStyle(color: Colors.white),
          ),
        ],
      ),
    );

    _fToast.showToast(
        child: toast,
        toastDuration: Duration(seconds: 2),
        positionedToastBuilder: (context, child) {
          return Positioned(child: child, top: 110, left: 0, right: 0);
        });
  }
}
