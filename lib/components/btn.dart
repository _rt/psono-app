import 'package:flutter/material.dart';

class Btn extends StatelessWidget {
  final String text;
  final VoidCallback onPressed;
  final Color color;
  final Color textColor;

  Btn(
      {this.text,
      this.onPressed,
      this.color,
      this.textColor = const Color(0xFF333333)});

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5),
      ),
      onPressed: () {
        onPressed();
      },
      padding: EdgeInsets.all(12),
      color: color,
      child: Text(text, style: TextStyle(color: textColor)),
    );
  }
}
