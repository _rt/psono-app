import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter/scheduler.dart';

typedef void CallbackFunction(String search);

class SliverAppSearchBar extends StatefulWidget {
  static String tag = 'datastore-screen';

  final String title;
  final CallbackFunction onSearch;
  final String defaultSearch;

  SliverAppSearchBar({
    this.title,
    this.onSearch,
    this.defaultSearch,
  });

  @override
  _SliverAppSearchBarState createState() => _SliverAppSearchBarState();
}

class _SliverAppSearchBarState extends State<SliverAppSearchBar> {
  Widget appBarTitle;
  Icon actionIcon;
  TextEditingController search;

  @override
  void initState() {
    super.initState();
    search = new TextEditingController();
    search.addListener(() {
      if (search.text.isEmpty) {
        this.widget.onSearch(null);
      } else {
        this.widget.onSearch(search.text);
      }
    });
  }

  @override
  void dispose() {
    search?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SchedulerBinding.instance.addPostFrameCallback((_) async {
      if (this.widget.defaultSearch != null &&
          this.widget.defaultSearch.length > 0) {
        search.text = this.widget.defaultSearch;
        actionIcon = new Icon(Icons.close);
        appBarTitle = new TextField(
          autofocus: true,
          controller: search,
          style: new TextStyle(
            color: Colors.black,
            fontSize: 20.0,
          ),
          decoration: new InputDecoration(
              prefixIcon: new Icon(Icons.search, color: Colors.black),
              hintText: FlutterI18n.translate(context, "SEARCH"),
              hintStyle: new TextStyle(color: Colors.black)),
        );
      }
    });

    if (actionIcon == null) {
      actionIcon = new Icon(Icons.search);
    }
    if (appBarTitle == null) {
      appBarTitle = new Text(this.widget.title);
    }
    return SliverAppBar(
//      automaticallyImplyLeading: false,
//      floating: true,
//      titleSpacing: 0,
//      elevation: 4.0,
      backgroundColor: Colors.white,
      pinned: true,
      title: appBarTitle,
      actions: <Widget>[
        new IconButton(
          icon: actionIcon,
          onPressed: () {
            setState(() {
              if (this.actionIcon.icon == Icons.search) {
                this.actionIcon = new Icon(Icons.close);
                this.appBarTitle = new TextField(
                  autofocus: true,
                  controller: search,
                  style: new TextStyle(
                    color: Colors.black,
                    fontSize: 20.0,
                  ),
                  decoration: new InputDecoration(
                      prefixIcon: new Icon(Icons.search, color: Colors.black),
                      hintText: FlutterI18n.translate(context, "SEARCH"),
                      hintStyle: new TextStyle(color: Colors.black)),
                );
              } else {
                this.actionIcon = new Icon(Icons.search);
                this.appBarTitle = new Text(
                  this.widget.title,
                );
                search.clear();
              }
            });
          },
        ),
      ],
    );
  }
}
