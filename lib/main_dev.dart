import 'package:psono/app_config.dart';
import 'package:psono/main_core.dart';
import 'package:psono/services/sentry.dart';
import 'package:flutter/material.dart';
import 'dart:async';

Future<Null> main() async {
  // This captures errors reported by the Flutter framework.
  FlutterError.onError = (FlutterErrorDetails details) {
    if (isInDebugMode) {
      // In development mode, simply print to console.
      FlutterError.dumpErrorToConsole(details);
    } else {
      // In production mode, report to the application zone to report to
      // Sentry.
      Zone.current.handleUncaughtError(details.exception, details.stack);
    }
  };

  runZoned<Future<void>>(() async {
    var configuredApp = new AppConfig(
      appName: 'Psono Dev',
      flavorName: 'development',
      child: new MyApp(),
    );
    runApp(configuredApp);
  }, onError: (error, stackTrace) {
    // Whenever an error occurs, call the `reportError` function. This sends
    // Dart errors to the dev console or Sentry depending on the environment.
    reportError(error, stackTrace);
  });
}
