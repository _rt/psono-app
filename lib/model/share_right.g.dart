// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'share_right.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ShareRight _$ShareRightFromJson(Map<String, dynamic> json) {
  return ShareRight(
    read: json['read'] as bool,
    write: json['write'] as bool,
    grant: json['grant'] as bool,
    delete: json['delete'] as bool,
  );
}

Map<String, dynamic> _$ShareRightToJson(ShareRight instance) =>
    <String, dynamic>{
      'read': instance.read,
      'write': instance.write,
      'grant': instance.grant,
      'delete': instance.delete,
    };
