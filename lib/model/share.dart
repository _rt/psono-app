import 'dart:convert';
import 'dart:typed_data';

import 'package:psono/model/datastore.dart';
import 'package:psono/services/converter.dart';
import 'package:psono/services/manager_datastore_password.dart'
    as managerDatastorePassword;
import 'package:psono/services/manager_share.dart' as managerShare;
import 'package:json_annotation/json_annotation.dart';
import './share_right.dart';

part 'share.g.dart';

/// Represents a datastore
@JsonSerializable()
class Share {
  Share({
    this.shareId,
    this.shareSecretKey,
    this.folder,
    this.item,
    this.rights,
  });

  final String shareId;
  @JsonKey(fromJson: fromHex, toJson: toHex)
  final Uint8List shareSecretKey;
  final Folder folder;
  final Item item;
  final ShareRight rights;

  save() async {
    Item itemCopy;
    Folder folderCopy;
    String jsonContent;
    if (this.item != null) {
      itemCopy = (this.item == null) ? null : this.item.clone();
      jsonContent = jsonEncode(itemCopy);
    } else {
      folderCopy = (this.folder == null) ? null : this.folder.clone();
      managerDatastorePassword.hideSubShareContent(folderCopy);
      jsonContent = jsonEncode(folderCopy);
    }

    return await managerShare.writeShare(
        this.shareId, jsonContent, this.shareSecretKey);
  }

  factory Share.fromJson(Map<String, dynamic> json) => _$ShareFromJson(json);

  Map<String, dynamic> toJson() => _$ShareToJson(this);
}
