import 'dart:typed_data';
import 'package:json_annotation/json_annotation.dart';
import 'package:psono/services/converter.dart';
import 'package:psono/services/manager_secret.dart' as managerSecret;

part 'secret.g.dart';

/// Represents a datastore
@JsonSerializable()
class SecretData {
  SecretData({
    this.type,
    this.applicationPasswordTitle,
    this.applicationPasswordUsername,
    this.applicationPasswordPassword,
    this.applicationPasswordNotes,
    this.totpTitle,
    this.totpCode,
    this.websitePasswordTitle,
    this.websitePasswordUrl,
    this.websitePasswordUsername,
    this.websitePasswordPassword,
    this.websitePasswordNotes,
    this.websitePasswordAutoSubmit,
    this.websitePasswordUrlFilter,
    this.noteTitle,
    this.noteNotes,
    this.fileTitle,
    this.file,
    this.fileId,
    this.fileShardId,
    this.fileRepositoryId,
    this.fileDestination,
    this.fileSecretKey,
    this.fileSize,
    this.fileChunks,
    this.mailGPGOwnKeyTitle,
    this.mailGPGOwnKeyEmail,
    this.mailGPGOwnKeyName,
    this.mailGPGOwnKeyPublic,
    this.mailGPGOwnKeyPrivate,
    this.mailGPGOwnKeyGenerateNew,
    this.mailGPGOwnKeyGenerateImportText,
    this.mailGPGOwnKeyEncryptMessaget,
    this.mailGPGOwnKeyDecryptMessaget,
    this.bookmarkTitle,
    this.bookmarkUrl,
    this.bookmarkNotes,
    this.bookmarkUrlFilter,
  });

  final String type;
  @JsonKey(name: 'application_password_title')
  final String applicationPasswordTitle;
  @JsonKey(name: 'application_password_username')
  final String applicationPasswordUsername;
  @JsonKey(name: 'application_password_password')
  final String applicationPasswordPassword;
  @JsonKey(name: 'application_password_notes')
  final String applicationPasswordNotes;
  @JsonKey(name: 'totp_title')
  final String totpTitle;
  @JsonKey(name: 'totp_code')
  final String totpCode;
  @JsonKey(name: 'website_password_title')
  final String websitePasswordTitle;
  @JsonKey(name: 'website_password_url')
  final String websitePasswordUrl;
  @JsonKey(name: 'website_password_username')
  final String websitePasswordUsername;
  @JsonKey(name: 'website_password_password')
  final String websitePasswordPassword;
  @JsonKey(name: 'website_password_notes')
  final String websitePasswordNotes;
  @JsonKey(name: 'website_password_auto_submit')
  final bool websitePasswordAutoSubmit;
  @JsonKey(name: 'website_password_url_filter')
  final String websitePasswordUrlFilter;
  @JsonKey(name: 'note_title')
  final String noteTitle;
  @JsonKey(name: 'note_notes')
  final String noteNotes;
  @JsonKey(name: 'file_title')
  final String fileTitle;
  @JsonKey(name: 'file')
  final String file;
  @JsonKey(name: 'file_id')
  final String fileId;
  @JsonKey(name: 'file_shard_id')
  final String fileShardId;
  @JsonKey(name: 'file_repository_id')
  final String fileRepositoryId;
  @JsonKey(name: 'file_destinations')
  final String fileDestination;
  @JsonKey(name: 'file_secret_key')
  final String fileSecretKey;
  @JsonKey(name: 'file_size')
  final String fileSize;
  @JsonKey(name: 'file_chunks')
  final String fileChunks;
  @JsonKey(name: 'mail_gpg_own_key_title')
  final String mailGPGOwnKeyTitle;
  @JsonKey(name: 'mail_gpg_own_key_email')
  final String mailGPGOwnKeyEmail;
  @JsonKey(name: 'mail_gpg_own_key_name')
  final String mailGPGOwnKeyName;
  @JsonKey(name: 'mail_gpg_own_key_public')
  final String mailGPGOwnKeyPublic;
  @JsonKey(name: 'mail_gpg_own_key_private')
  final String mailGPGOwnKeyPrivate;
  @JsonKey(name: 'mail_gpg_own_key_generate_new')
  final String mailGPGOwnKeyGenerateNew;
  @JsonKey(name: 'mail_gpg_own_key_generate_import_text')
  final String mailGPGOwnKeyGenerateImportText;
  @JsonKey(name: 'mail_gpg_own_key_encrypt_message')
  final String mailGPGOwnKeyEncryptMessaget;
  @JsonKey(name: 'mail_gpg_own_key_decrypt_message')
  final String mailGPGOwnKeyDecryptMessaget;
  @JsonKey(name: 'bookmark_title')
  final String bookmarkTitle;
  @JsonKey(name: 'bookmark_url')
  final String bookmarkUrl;
  @JsonKey(name: 'bookmark_notes')
  final String bookmarkNotes;
  @JsonKey(name: 'bookmark_url_filter')
  final String bookmarkUrlFilter;

  factory SecretData.fromJson(Map<String, dynamic> json) =>
      _$SecretDataFromJson(json);

  Map<String, dynamic> toJson() => _$SecretDataToJson(this);
}

/// Represents a datastore
@JsonSerializable()
class Secret {
  Secret({
    this.createDate,
    this.writeDate,
    this.secretId,
    this.type,
    this.data,
    this.callbackUrl,
    this.callbackUser,
    this.callbackPass,
    this.secretKey,
  });

  final String createDate;
  final String writeDate;
  String secretId;
  String type;
  dynamic data;
  String callbackUrl;
  String callbackUser;
  String callbackPass;
  @JsonKey(name: 'secret_key', fromJson: fromHex, toJson: toHex)
  Uint8List secretKey;

  factory Secret.fromJson(Map<String, dynamic> json) => _$SecretFromJson(json);

  Map<String, dynamic> toJson() => _$SecretToJson(this);

  save() async {
    return await managerSecret.writeSecret(
      this.secretId,
      this.secretKey,
      data,
      this.callbackUrl,
      this.callbackUser,
      this.callbackPass,
    );
  }
}
