class OTP {
  OTP({
    this.type,
    this.label,
    this.issuer,
    this.secret,
    this.period,
  });

  String type;
  String label;
  String issuer;
  String secret;
  int counter;
  int period;

  String getDescription() {
    String description = '';
    if (issuer != null) {
      description = description + issuer;
    }
    if (description != '' && label != null && label != '') {
      description = description + ':';
    }
    if (label != null && label != '') {
      description = description + label;
    }
    return description;
  }
}
