import 'dart:typed_data';
import 'package:flutter/foundation.dart';

import 'package:psono/model/config.dart';

class AppState {
  final String serverUrl;
  final Uint8List verifyKeyOld;
  final Uint8List verifyKey;
  final String username;
  //final String password;
  final String token;
  final Uint8List sessionSecretKey;
  final Uint8List secretKey;
  final Uint8List publicKey;
  final Uint8List privateKey;
  final String lockscreenPassphrase;
  final bool lockscreenEnabled;
  final bool complianceDisableDeleteAccount;
  final String userId;
  final String userEmail;
  final String userSauce;
  final Config config;
  final String passwordLength;
  final String lettersUppercase;
  final String lettersLowercase;
  final String numbers;
  final String specialChars;

  AppState({
    @required this.serverUrl,
    @required this.verifyKeyOld,
    @required this.verifyKey,
    @required this.username,
    //@required this.password,
    @required this.token,
    @required this.sessionSecretKey,
    @required this.secretKey,
    @required this.publicKey,
    @required this.privateKey,
    @required this.lockscreenPassphrase,
    @required this.lockscreenEnabled,
    @required this.complianceDisableDeleteAccount,
    @required this.userId,
    @required this.userEmail,
    @required this.userSauce,
    @required this.config,
    @required this.passwordLength,
    @required this.lettersUppercase,
    @required this.lettersLowercase,
    @required this.numbers,
    @required this.specialChars,
  });

  factory AppState.initialState() {
    String serverUrl = '';
    Uint8List verifyKeyOld = new Uint8List(0);
    Uint8List verifyKey = new Uint8List(0);
    String username = '';
    //String password = '';
    String token = '';
    Uint8List sessionSecretKey = new Uint8List(0);
    Uint8List secretKey = new Uint8List(0);
    Uint8List publicKey = new Uint8List(0);
    Uint8List privateKey = new Uint8List(0);
    String lockscreenPassphrase = '';
    bool lockscreenEnabled = false;
    bool complianceDisableDeleteAccount = false;
    String userId = '';
    String userEmail = '';
    String userSauce = '';
    Config config = new Config();
    String passwordLength = '';
    String lettersUppercase = '';
    String lettersLowercase = '';
    String numbers = '';
    String specialChars = '';

    return AppState(
      serverUrl: serverUrl,
      verifyKeyOld: verifyKeyOld,
      verifyKey: verifyKey,
      username: username,
      //password: password,
      token: token,
      sessionSecretKey: sessionSecretKey,
      secretKey: secretKey,
      publicKey: publicKey,
      privateKey: privateKey,
      lockscreenPassphrase: lockscreenPassphrase,
      lockscreenEnabled: lockscreenEnabled,
      complianceDisableDeleteAccount: complianceDisableDeleteAccount,
      userId: userId,
      userEmail: userEmail,
      userSauce: userSauce,
      config: config,
      passwordLength: passwordLength,
      lettersUppercase: lettersUppercase,
      lettersLowercase: lettersLowercase,
      numbers: numbers,
      specialChars: specialChars,
    );
  }
}
