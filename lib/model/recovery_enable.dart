import 'dart:typed_data';

class RecoveryEnable {
  final Uint8List userPrivateKey;
  final Uint8List userSecretKey;
  final String userSauce;
  final Uint8List verifierPublicKey;
  final int verifierTimeValid;

  const RecoveryEnable(
    this.userPrivateKey,
    this.userSecretKey,
    this.userSauce,
    this.verifierPublicKey,
    this.verifierTimeValid,
  );
}
