import 'package:json_annotation/json_annotation.dart';
import 'package:psono/services/helper.dart';

part 'login_info.g.dart';

@JsonSerializable()
class LoginInfo {
  LoginInfo({
    this.samlTokenId,
    this.oidcTokenId,
    this.username,
    this.authkey,
    this.deviceTime,
    this.deviceFingerprint,
    this.deviceDescription,
    this.password,
    this.clientType,
  });

  @JsonKey(name: 'saml_token_id')
  final String samlTokenId;
  @JsonKey(name: 'oidc_token_id')
  final String oidcTokenId;
  final String username;
  final String authkey;
  @JsonKey(name: 'device_time', fromJson: isoToDateTime, toJson: dateTimeToIso)
  DateTime deviceTime;
  @JsonKey(name: 'device_fingerprint')
  final String deviceFingerprint;
  @JsonKey(name: 'device_description')
  final String deviceDescription;
  final String password;
  @JsonKey(name: 'client_type')
  final String clientType;

  factory LoginInfo.fromJson(Map<String, dynamic> json) =>
      _$LoginInfoFromJson(json);

  Map<String, dynamic> toJson() => _$LoginInfoToJson(this);
}
