import 'package:flutter/widgets.dart';

class DropDownMenuItem {
  DropDownMenuItem({
    this.icon,
    this.text,
    this.hideOffline,
    this.hideOnNotWrite,
    this.onclick,
  });

  final IconData icon;
  final String text;
  final bool hideOffline;
  final bool hideOnNotWrite;
  final Function onclick;
}

class BlueprintField {
  BlueprintField({
    this.name,
    this.field,
    this.type,
    this.title,
    this.validationType,
    this.placeholder,
    this.required = false,
    this.position,
    this.onChange,
    this.dropDownMenuItems,
    this.hidden = false,
    this.readonly = false,
    this.onClick,
    this.classname,
    this.errorMessageRequired,
  });

  final String name;
  final String field;
  final String type;
  final String title;
  final String validationType;
  final String placeholder;
  bool required;
  final String position;
  final String onChange;
  final List<DropDownMenuItem> dropDownMenuItems;
  bool hidden;
  bool readonly;
  final String onClick;
  final String classname;
  final String errorMessageRequired;
}

class Blueprint {
  Blueprint({
    this.id,
    this.name,
    this.icon,
    this.titleField,
    this.urlfilterField,
    this.autosubmitField,
    this.search,
    this.fields,
  });

  final String id;
  final String name;
  final IconData icon;
  final String titleField;
  final String urlfilterField;
  final String autosubmitField;
  final List<String> search;
  final List<BlueprintField> fields;
}
