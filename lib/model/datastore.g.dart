// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'datastore.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Item _$ItemFromJson(Map<String, dynamic> json) {
  return Item(
    id: json['id'] as String,
    name: json['name'] as String,
    deleted: json['deleted'] as bool,
    type: json['type'] as String,
    urlfilter: json['urlfilter'] as String,
    secretId: json['secret_id'] as String,
    fileChunks: (json['file_chunks'] as Map<String, dynamic>)?.map(
      (k, e) => MapEntry(int.parse(k), e as String),
    ),
    fileId: json['file_id'] as String,
    fileTitle: json['file_title'] as String,
    fileRepositoryId: json['file_repository_id'] as String,
    fileShardId: json['file_shard_id'] as String,
    fileSecretKey: fromHex(json['file_secret_key'] as String),
    secretKey: fromHex(json['secret_key'] as String),
    shareId: json['share_id'] as String,
    shareSecretKey: fromHex(json['share_secret_key'] as String),
  );
}

Map<String, dynamic> _$ItemToJson(Item instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('id', instance.id);
  writeNotNull('name', instance.name);
  writeNotNull('deleted', instance.deleted);
  writeNotNull('type', instance.type);
  writeNotNull('urlfilter', instance.urlfilter);
  writeNotNull('secret_id', instance.secretId);
  writeNotNull('file_chunks',
      instance.fileChunks?.map((k, e) => MapEntry(k.toString(), e)));
  writeNotNull('file_id', instance.fileId);
  writeNotNull('file_title', instance.fileTitle);
  writeNotNull('file_repository_id', instance.fileRepositoryId);
  writeNotNull('file_shard_id', instance.fileShardId);
  writeNotNull('file_secret_key', toHex(instance.fileSecretKey));
  writeNotNull('secret_key', toHex(instance.secretKey));
  writeNotNull('share_id', instance.shareId);
  writeNotNull('share_secret_key', toHex(instance.shareSecretKey));
  return val;
}

ShareLocation _$ShareLocationFromJson(Map<String, dynamic> json) {
  return ShareLocation(
    secretKey: fromHex(json['secret_key'] as String),
    paths: (json['paths'] as List)
        ?.map((e) => (e as List)?.map((e) => e as String)?.toList())
        ?.toList(),
  );
}

Map<String, dynamic> _$ShareLocationToJson(ShareLocation instance) =>
    <String, dynamic>{
      'secret_key': toHex(instance.secretKey),
      'paths': instance.paths,
    };

Folder _$FolderFromJson(Map<String, dynamic> json) {
  return Folder(
    id: json['id'] as String,
    datastoreId: json['datastore_id'] as String,
    folders: (json['folders'] as List)
        ?.map((e) =>
            e == null ? null : Folder.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    items: (json['items'] as List)
        ?.map(
            (e) => e == null ? null : Item.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    name: json['name'] as String,
    deleted: json['deleted'] as bool,
    shareId: json['share_id'] as String,
    shareSecretKey: fromHex(json['share_secret_key'] as String),
    shareIndex: (json['share_index'] as Map<String, dynamic>)?.map(
      (k, e) => MapEntry(k,
          e == null ? null : ShareLocation.fromJson(e as Map<String, dynamic>)),
    ),
  );
}

Map<String, dynamic> _$FolderToJson(Folder instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('id', instance.id);
  writeNotNull('datastore_id', instance.datastoreId);
  writeNotNull('folders', instance.folders);
  writeNotNull('items', instance.items);
  writeNotNull('name', instance.name);
  writeNotNull('deleted', instance.deleted);
  writeNotNull('share_id', instance.shareId);
  writeNotNull('share_secret_key', toHex(instance.shareSecretKey));
  writeNotNull('share_index', instance.shareIndex);
  return val;
}

Datastore _$DatastoreFromJson(Map<String, dynamic> json) {
  return Datastore(
    datastoreId: json['datastoreId'] as String,
    type: json['type'] as String,
    description: json['description'] as String,
    secretKey: fromHex(json['secretKey'] as String),
    isDefault: json['isDefault'] as bool,
    data: json['data'] == null
        ? null
        : Folder.fromJson(json['data'] as Map<String, dynamic>),
    dataKV: json['dataKV'],
  );
}

Map<String, dynamic> _$DatastoreToJson(Datastore instance) => <String, dynamic>{
      'datastoreId': instance.datastoreId,
      'type': instance.type,
      'data': instance.data,
      'dataKV': instance.dataKV,
      'description': instance.description,
      'secretKey': toHex(instance.secretKey),
      'isDefault': instance.isDefault,
    };
