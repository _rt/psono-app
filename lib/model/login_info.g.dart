// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_info.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LoginInfo _$LoginInfoFromJson(Map<String, dynamic> json) {
  return LoginInfo(
    samlTokenId: json['saml_token_id'] as String,
    oidcTokenId: json['oidc_token_id'] as String,
    username: json['username'] as String,
    authkey: json['authkey'] as String,
    deviceTime: isoToDateTime(json['device_time'] as String),
    deviceFingerprint: json['device_fingerprint'] as String,
    deviceDescription: json['device_description'] as String,
    password: json['password'] as String,
    clientType: json['client_type'] as String,
  );
}

Map<String, dynamic> _$LoginInfoToJson(LoginInfo instance) => <String, dynamic>{
      'saml_token_id': instance.samlTokenId,
      'oidc_token_id': instance.oidcTokenId,
      'username': instance.username,
      'authkey': instance.authkey,
      'device_time': dateTimeToIso(instance.deviceTime),
      'device_fingerprint': instance.deviceFingerprint,
      'device_description': instance.deviceDescription,
      'password': instance.password,
      'client_type': instance.clientType,
    };
