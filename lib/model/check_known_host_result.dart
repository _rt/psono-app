import 'dart:typed_data';

class CheckKnownHostResult {
  CheckKnownHostResult({
    this.status,
    this.verifyKeyOld,
  });

  final String status;
  final Uint8List verifyKeyOld;
}
