import 'package:json_annotation/json_annotation.dart';

part 'config.g.dart';

@JsonSerializable()
class BackendServer {
  BackendServer({
    this.title,
    this.url,
  });

  @JsonKey(name: 'title')
  final String title;
  @JsonKey(name: 'url')
  final String url;

  factory BackendServer.fromJson(Map<String, dynamic> json) =>
      _$BackendServerFromJson(json);

  Map<String, dynamic> toJson() => _$BackendServerToJson(this);
}

@JsonSerializable()
class SamlProvider {
  SamlProvider({
    this.title,
    this.providerId,
    this.buttonName,
  });

  @JsonKey(name: 'title')
  final String title;
  @JsonKey(name: 'provider_id')
  final int providerId;
  @JsonKey(name: 'button_name')
  final String buttonName;

  factory SamlProvider.fromJson(Map<String, dynamic> json) =>
      _$SamlProviderFromJson(json);

  Map<String, dynamic> toJson() => _$SamlProviderToJson(this);
}

@JsonSerializable()
class OidcProvider {
  OidcProvider({
    this.title,
    this.providerId,
    this.buttonName,
  });

  @JsonKey(name: 'title')
  final String title;
  @JsonKey(name: 'provider_id')
  final int providerId;
  @JsonKey(name: 'button_name')
  final String buttonName;

  factory OidcProvider.fromJson(Map<String, dynamic> json) =>
      _$OidcProviderFromJson(json);

  Map<String, dynamic> toJson() => _$OidcProviderToJson(this);
}

@JsonSerializable()
class MoreLink {
  MoreLink({
    this.href,
    this.title,
    this.className,
  });

  @JsonKey(name: 'href')
  final String href;
  @JsonKey(name: 'title')
  final String title;
  @JsonKey(name: 'class')
  final String className;

  factory MoreLink.fromJson(Map<String, dynamic> json) =>
      _$MoreLinkFromJson(json);

  Map<String, dynamic> toJson() => _$MoreLinkToJson(this);
}

@JsonSerializable()
class ConfigJson {
  ConfigJson({
    this.allowCustomServer,
    this.allowRegistration,
    this.allowLostPassword,
    this.authenticationMethods,
    this.baseUrl,
    this.moreLinks,
    this.backendServers,
    this.samlProvider,
    this.oidcProvider,
  });

  @JsonKey(name: 'allow_custom_server')
  final bool allowCustomServer;
  @JsonKey(name: 'allow_registration')
  final bool allowRegistration;
  @JsonKey(name: 'allow_lost_password')
  final bool allowLostPassword;
  @JsonKey(name: 'authentication_methods')
  final List<String> authenticationMethods;
  @JsonKey(name: 'base_url')
  final String baseUrl;

  @JsonKey(name: 'more_links')
  final List<MoreLink> moreLinks;
  @JsonKey(name: 'backend_servers')
  final List<BackendServer> backendServers;
  @JsonKey(name: 'saml_provider')
  final List<SamlProvider> samlProvider;
  @JsonKey(name: 'oidc_provider')
  final List<OidcProvider> oidcProvider;

  factory ConfigJson.fromJson(Map<String, dynamic> json) =>
      _$ConfigJsonFromJson(json);

  Map<String, dynamic> toJson() => _$ConfigJsonToJson(this);
}

@JsonSerializable()
class Config {
  Config({
    this.configJson,
  });

  @JsonKey(name: 'ConfigJson')
  final ConfigJson configJson;

  factory Config.fromJson(Map<String, dynamic> json) => _$ConfigFromJson(json);

  Map<String, dynamic> toJson() => _$ConfigToJson(this);
}
