import 'package:flutter/material.dart';

// Useful resources:
// http://mcg.mbitson.com/#!?mcgpalette0=%232dbb93

const MaterialColor primarySwatch = MaterialColor(
  _primarySwatchValue,
  <int, Color>{
    50: Color(0xFFE6F7F2),
    100: Color(0xFFC0EBDF),
    200: Color(0xFF96DDC9),
    300: Color(0xFF6CCFB3),
    400: Color(0xFF4DC5A3),
    500: Color(_primarySwatchValue),
    600: Color(0xFF28B58B),
    700: Color(0xFF22AC80),
    800: Color(0xFF1CA476),
    900: Color(0xFF119664),
  },
);
const int _primarySwatchValue = 0xFF2dbb93;

class NoAnimationMaterialPageRoute<T> extends MaterialPageRoute<T> {
  NoAnimationMaterialPageRoute({
    @required WidgetBuilder builder,
    RouteSettings settings,
    bool maintainState = true,
    bool fullscreenDialog = false,
  }) : super(
          builder: builder,
          maintainState: maintainState,
          settings: settings,
          fullscreenDialog: fullscreenDialog,
        );

  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation, Widget child) {
    return child;
  }
}
